## Documentation API externe : Adhésion

*Une documentation complète et automatique de l'API est possible mais il est plus simple de partager aux utilisateurs
extérieurs ce document*



* **Endpoint API:** https://api-adhesion.asso-insa-lyon.fr/va_check  
**Protection de l'endpoint:**  
Voir un responsable pour obtenir les credentials du client Keycloak pour votre API  
**Role Keycloak:**  
"24h"   
<br></br>
Allow: POST, OPTIONS  
Content-Type: application/json  
Vary: Accept  
<br></br> 
*Request exemple:*  
{"first_name": "Pierre","last_name":"Jacques","card":"c000000000001"}  
*Response exemple:*  
{"first_name":"Pierre","last_name":"Jacques","has_valid_membership": true}   
<br></br>  
* **Endpoint permanencier:** https://api-adhesion.asso-insa-lyon.fr/24h  
**Protection de l'endpoint:**  
Token keycloak lié à un compte Keycloak VA avec le bon rôle. Voir avec un responsable pour obtenir un compte et/ou le rôle   
**Role Keycloak:**  
"24h-permanencier"  
<br></br>
Allow: POST, OPTIONS  
Content-Type: application/json  
Vary: Accept  
<br></br>
*Request exemple:*  
{"card": "c000000000001"}  
*Response exemple:*  
{"first_name":"Pierre","last_name":"Jacques","email":"pierre.jacques@insa-lyon.fr","has_valid_membership": true}
