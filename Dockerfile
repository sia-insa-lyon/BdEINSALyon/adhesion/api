FROM python:3.6

WORKDIR /app

COPY requirements.txt /app
RUN pip3 install -r requirements.txt
COPY . /app
VOLUME /app/staticfiles

RUN chmod +x bash/run-prod.sh

EXPOSE 8000
ENV DATABASE_URL postgres://postgresql:postgresql@db:5432/adhesion2

CMD /app/bash/run-prod.sh
