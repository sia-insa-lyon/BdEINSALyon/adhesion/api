# Adhesion API

Python API for student membership management software. It holds 
the data logic and provide API for different frontends.

```mermaid
graph TB;
kc[Keycloak authentification]---|OpenID Connect|fp
kc---|OpenID Connect|fa
kc---|OpenID Connect|ft
fp[Public public] -->|HTTP ReST| api
ft[Laverie] -->|HTTP ReST| api
fa[Admin frontend]-->|HTTP ReST| api
api[Serveur adhesion API] ---|SQL| db[Base de donnees]
api -->|HTTP ReST| kcapi(Keycloak Administration API)
```

# Getting starting

## 1. Download images and start containers

   To create a dev stack use the docker-compose file: `local-compose.yml` 

<details>
  <summary>Venv user ?</summary>

  --- 
  If you prefer to develop in a venv rather than with the Docker container, you can launch only Keycloak with
  the command :

  ```bash
  docker-compose -f local-compose.yml up -d keycloak
  ```
  ---
</details>

```bash
git clone git@gitlab.com:sia-insa-lyon/BdEINSALyon/adhesion/api.git
cd api
docker compose -f local-compose.yml up api
```
*(wait a little, it's quite long the first time...)*

The docker compose has 3 services:

|                      | Port (can be changed) | Description                                                                                         |
|----------------------|-----------------------|-----------------------------------------------------------------------------------------------------|
| Keycloak             | 8080                  | Open-source SSO ([link](https://www.keycloak.org/))                                                 |
| PostgreSQL           | 5432                  | Open-source SQL database ([link](https://www.postgresql.org/))                                      |
| Gunicorn with Django | 8000                  | Gunicorn HTTP server ([link](https://gunicorn.org/)) Django([link](https://www.djangoproject.com/)) |

The current file is mounted in volume on the API container. Gunicorn is configured in autoreload which allows to take in real time your code changes on your host device.

The instantiation of Keycloak is done by creating a realm and inserting the users and other clients from the [json file](keycloak_realm_asso-insa-lyon.json)

### 💚 Checkpoint:
At this step you should see:
- Django interface [localhost:8000](http://localhost:8000)
- Keycloak interface [localhost:8080](http://localhost:8080)

## 2. Populating the database

```bash
docker compose -f local-compose.yml exec api bash
``` 

```bash
python manage.py migrate
python manage.py seeddb
python manage.py createsuperuser
```

### 💚 Checkpoint:
At this step you should see:
- Members on Django's admin interface [localhost:8000/admin](http://localhost:8000/admin)


## 3. Enjoy 
Have fun with the API ! 👍  
If you need help, please ping a SIA member.

___


# MercaNET

## Format of *transactionReferences*

Only alphanumeric characters can be used with Mercanet, so the separation between fields is done with
`U` and `u`.

The format is as follows:  
``member_id U id U membership U transaction_id UU day_date u month_date u time_date u minute_date uUu hash``  
(truncated hash for entropy)

Details can be found [here](mercanet/utils.py)

i.e: `1U2U3UU13u06u16u23uUu77de68daec`

--- 
# Keycloak

### For JWT authentication


```mermaid
graph TB;
subgraph Serveur Keycloak;
Keycloak[profil Keycloak];
end;
subgraph Serveur Adhesion;
Keycloak--adhesionUserId-->User;
Keycloak--memberId-->Member;
User-->Member;
Member-->StudentProfile;
Member-->Adhesion;
Adhesion-->TypeAdhesion;
end;
```



We synchronize Keycloak database with Django database thanks to:

- 'Adherent' ID in Django
   * name: adhesion_member_id
   * *Mapper Type*: **User Attribute** 
   * *Token Claim Name* : `adhesion_member_id`
   * *User Attribute* : `memberId`
   * *Claim JSON Type*: `String`
 - User ID Django
   * name: adhesion_member_id
   * *Mapper Type*: **User Attribute**
   * *Token Claim Name* : `adhesion_user_id`
   * *User Attribute* : `adhesionUserId`
   * *Claim JSON Type*: `String`



--- 
# Minio
## Static files with Minio
### Creating the account
Here, `myminio` is the minio server configured in your command line.
```shell script
mc admin user add myminio adhesionstatic password123
mc admin policy set myminio readwrite user=adhesionstatic
mc mb myminio/adhesion-staticfiles
mc policy set download myminio/adhesion-staticfiles #on autorise le téléchargement public
```
### Subtleties
Minio variables must be set when running ``manage.py collectstatic``, as this is the command that sends files to the bucket.
### Keycloak
Please note that the Keycloak Admin API allows you to update a user via PUT.
However it is not smart enough to merge attributes changes.
The attributes of the user are therefore overwritten.
### Environment variables
```shell script
USE_MINIO_STATICFILES=True
MINIO_ACCESS_KEY=adhesionstatic
MINIO_SECRET_KEY=password123
MINIO_ENDPOINT=s3.bde-insa-lyon.fr
STATIC_BUCKET=adhesion-staticfiles
MINIO_STORAGE_USE_HTTPS=True
```
## Backups with Minio
Be careful, it's the same variable names but not the same content.
The idea is that the static files should be read-write; however, the backups should be write-only.
```shell script
MINIO_ACCESS_KEY=username
MINIO_SECRET_KEY=password
MINIO_ENDPOINT=s3.bde-insa-lyon.fr    # without 'http' ou 'https'!
BUCKET_NAME=adhesion-db-backups
```
## Making a backup
```shell script
docker run -e MINIO_ACCESS_KEY=username MINIO_SECRET_KEY=password \
  MINIO_ENDPOINT=s3.bde-insa-lyon.fr BUCKET_NAME=adhesion-db-backups \
    --rm adhesion-api /app/bash/backup-minio.sh
docker run -e MINIO_ACCESS_KEY=petitS3 -e MINIO_SECRET_KEY=oiossole -e MINIO_ENDPOINT=s3.home.ribes.ovh --rm -e DATABASE_URL='sqlite:///' jeanribes/adhesion-api /bin/sh -c "/app/manage.py migrate && /app/bash/backup-minio.sh"

```
---

# Troubleshooting


- **Price**: When registering online, the user does not choose the type of membership, the server 
chooses the cheapest type of membership for the user.
It is therefore necessary to have a well established system of conditions.

- **Admin access**: Users logging in from Keycloak (AzureAD or manually) with a "staff" role are given permission to use
endpoints, and a django user account is created for them on their first
on their first login. However, their User model is not marked as *is_staff* or *is_superuser*!
They cannot log in to the django admin!


<details>

  <summary> Old documentation </summary>

To launch the project for developpment, run in the project root
```
sh bash/start-dev.sh
```
This will create a suitable Virtuelenv if needed, and populate the database with example data.
## Conception

This application is based on a API written with Django Rest Framework.
Client applications request data (or edit) through REST requests. The
application authentificate user through a OAuth token.

### Migration procedure from previous adhesion database

Adhesion were previously a RubyOnRails application with a PostgreSQL
database, but the two apps are not compatible directly. You have to 
follow this procedure to migrate data from MARSU to Adhesion:

1. Extract the database from a backup to a running PostgreSQL server
2. Connect to the PostgreSQL with 
[Datagrip](https://www.jetbrains.com/datagrip/), a JetBrains app
3. Export the following tables to csv in the same folder :
    * cards to `cards.csv`
    * departments to `departments.csv`
    * memberships to `memberships.csv`
    * payments to `payments.csv`
    * student to `students.csv`
    * study_years to `study_years.csv`
4. Run the command `python ./manage.py importoldadhesion <folder of csv>`
   Take care that the DB is clean before, unless the script will 
   refuse to work. You can run `python ./manage.py cleandb` to drop.

</details>

## License

This software is provided under GNU AGPL V3 license

### Contributors

```
© 2018 Philippe VIENNE <philippegeek@gmail.com>
© 2020 Jean RIBES <jean@ribes.ovh>
© 2021-2022 Baptiste LALANNE <baptiste.lalanne.pro@protonmail.com>
```