from django.contrib import admin
from adhesion.api import models
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export.fields import Field

class HiddenModelAdmin(admin.ModelAdmin):
    def get_model_perms(self, *args, **kwargs):
        perms = admin.ModelAdmin.get_model_perms(self, *args, **kwargs)
        perms['list_hide'] = True
        return {}


@admin.register(models.Card)
class CardAdmin(admin.ModelAdmin):
    pass


class CardsInline(admin.TabularInline):
    model = models.Card


class MembershipInline(admin.TabularInline):
    model = models.Membership

class MemberResource(resources.ModelResource):
    has_valid_membership = Field(attribute='has_valid_membership')
    class Meta:
        model = models.Member
        fields = ('id', 'first_name', 'last_name', 'email', 'phone', 'has_valid_membership', 'has_valid_card')

@admin.register(models.Member)
class MemberAdmin(ImportExportModelAdmin):
    resource_class = MemberResource
    list_display = ('id', 'first_name', 'last_name', 'email', 'phone', 'has_valid_membership', 'has_valid_card')
    search_fields = ('first_name', 'last_name', 'student_profile__student_number')
    raw_id_fields = ('student_profile',)
    list_filter = ('category', 'gender', 'student_profile__department__name', 'student_profile__study_year__name')
    inlines = [MembershipInline, CardsInline]
    list_per_page = 25


class MembershipResource(resources.ModelResource):
    class Meta:
        model = models.Membership
        fields = ('id', 'member__first_name', 'member__last_name', 'membership', 'valid', 'payment_method', 'updated_at','created_by')

@admin.register(models.Membership)
class MembershipAdmin(ImportExportModelAdmin):
    list_display = ('id', 'member', 'membership', 'valid', 'payment_method', 'updated_at','created_by')
    list_filter = ('payment_method', 'membership__name','created_by')
    search_fields = ('member__first_name', 'member__last_name', 'member__student_profile__student_number')
    raw_id_fields = ('member', 'membership')
    list_per_page = 25
    resource_class = MembershipResource





class TypeConditionInline(admin.StackedInline):
    model = models.MembershipType.conditions.through
    extra = 1


@admin.register(models.MembershipType)
class MembershipTypeAdmin(admin.ModelAdmin):
    fields = ('name', 'description', 'price_ht', 'price_ttc', 'enabled', 'not_valid_before', 'not_valid_after', 'conditions')
    list_display = ('name', 'description', 'price_ht', 'price_ttc', 'enabled', 'not_valid_before', 'not_valid_after')
    search_fields = ('name',)
    list_filter = ('price_ttc', 'enabled', 'not_valid_before')


@admin.register(models.TypeCondition)
class TypeConditionAdmin(HiddenModelAdmin, admin.ModelAdmin):
    pass


@admin.register(models.StudyYear)
class StudyYearAdmin(admin.ModelAdmin):
    list_display = ('name', 'year', 'order')
    list_filter = ('year',)
    search_fields = ('name', 'year')


@admin.register(models.StudyDepartment)
class StudyDepartmentAdmin(admin.ModelAdmin):
    list_display = ('short_name', 'name', 'school')
    list_filter = ('school__name',)
    search_fields = ('short_name', 'name')


@admin.register(models.StudySchool)
class StudySchoolAdmin(admin.ModelAdmin):
    list_display = ('short_name', 'name')
    search_fields = ('short_name', 'name')


@admin.register(models.StudentProfile)
class StudentProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'member', 'student_number', 'school', 'department', 'study_year', 'updated_at')
    search_fields = ('member__first_name', 'member__last_name', 'student_number')
    list_filter = ('member__gender', 'updated_at', 'department__name', 'study_year__name', 'school__name')
    list_per_page = 25
