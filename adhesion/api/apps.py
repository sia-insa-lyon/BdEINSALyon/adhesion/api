from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'adhesion.api'
    verbose_name = 'Données d\'Adhésion'
