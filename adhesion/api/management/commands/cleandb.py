from django.core.management.base import BaseCommand, CommandError

from adhesion.api.models import StudyYear, StudySchool, StudyDepartment, StudentProfile, Member, Membership, \
    MembershipType, TypeCondition


class Command(BaseCommand):
    help = 'Import previous adhesion version data'

    def add_arguments(self, parser):
        # parser.add_argument('data_folder', type=str)
        pass

    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Deleting...'))
        Membership.objects.all().delete()
        TypeCondition.objects.all().delete()
        MembershipType.objects.all().delete()
        Member.objects.all().delete()
        StudentProfile.objects.all().delete()
        StudyDepartment.objects.all().delete()
        StudyYear.objects.all().delete()
        StudySchool.objects.all().delete()

        self.stdout.write(self.style.SUCCESS('Successfully deleted!'))
