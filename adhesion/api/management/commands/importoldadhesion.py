import csv

import os
from django.core.management.base import BaseCommand

from adhesion.api.models import  Member

class Command(BaseCommand):
    help = 'Import previous adhesion version data'

    def add_arguments(self, parser):
        parser.add_argument('data_folder', type=str)

    def handle(self, *args, **options):

        with open(os.path.join(options['data_folder'], 'students.csv'), newline='\n') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            for r in reader:
                gender = r[1]
                first_name = r[2]
                last_name = r[3]
                email = r[4]
                phone ='+' + r[6]
                if phone == '':
                    phone = None
                birthday = r[7]
                if birthday == '':
                    birthday = None

                member = Member(first_name=first_name, last_name=last_name, email=email, gender=gender,
                                phone=phone, birthday=birthday)
                member.save()

        self.stdout.write(self.style.SUCCESS('Successfully imported!'))
