from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.utils.datetime_safe import datetime, date

from adhesion.api.models import StudyYear, StudySchool, StudyDepartment, StudentProfile, Member, Membership, \
    MembershipType, TypeCondition


class Command(BaseCommand):
    help = 'Seed database with example data'

    def add_arguments(self, parser):
        # parser.add_argument('data_folder', type=str)
        pass

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Creating data'))
        insa = StudySchool.objects.create(short_name='INSA', name='Institut national des sciences appliquées')
        cpe = StudySchool.objects.create(short_name='CPE', name='Chime Physique Éléctronique Lyon')
        imld = StudySchool.objects.create(short_name='Imld', name='Imladris')
        kzdm = StudySchool.objects.create(short_name='KZDM', name='Khâzad-dûm')

        a1 = StudyYear.objects.create(name='1A', year=1, order=1)
        a2 = StudyYear.objects.create(name='2A', year=2, order=2)
        a3 = StudyYear.objects.create(name='3A', year=3, order=3)
        a4 = StudyYear.objects.create(name='4A', year=4, order=4)
        a5 = StudyYear.objects.create(name='5A', year=5, order=5)
        doct = StudyYear.objects.create(name='Doctorant', year=6, order=6)

        tc = StudyDepartment.objects.create(short_name='TC', name='Télécom s&u', school=insa)
        tc.study_years.set([a3, a4, a5])

        pc = StudyDepartment.objects.create(short_name='FIMI', name='FIMI', school=insa)
        pc.study_years.set([a1, a2])
        iff = StudyDepartment.objects.create(short_name='IF', name='Ingénieur informaticien', school=insa)
        iff.study_years.set([a3, a4, a5])
        gm = StudyDepartment.objects.create(short_name='GM', name='Génie de la mécanique', school=insa)
        gm.study_years.set([a3, a4, a5])
        sgm = StudyDepartment.objects.create(short_name='SGM', name='Super Génie de la mécanique', school=insa)
        sgm.study_years.set([a3, a4, a5])

        ccppee = StudyDepartment.objects.create(short_name='CPE1', name='départ1', school=cpe)
        ccppee.study_years.set([a1, a2, a3, a4])

        arc = StudyDepartment.objects.create(short_name='arc', name="Tir à l'arc", school=imld)
        arc.study_years.set([a1, a2, a3, a4, a5, doct])
        mine = StudyDepartment.objects.create(short_name='mine', name="Minage", school=kzdm)
        forge = StudyDepartment.objects.create(short_name='forg', name="Métallurgie", school=kzdm)

        condition_non_1a = TypeCondition.objects.create(type=TypeCondition.YEAR_IS, value=1, negation=True,
                                                        description="non-1A")

        avantage = MembershipType.objects.create(name='VAvantage',
                                                 description="La carte VAvantages te permet d'accéder aux offres, "
                                                             "services et évènements du BdE ainsi qu'à des tarifs "
                                                             "très avantageux sur ceux de la Vie Associative.",
                                                 price_ht=18.87, price_ttc=20.00, nabled=True,
                                                 not_valid_after=date(year=2038, month=1, day=1),
                                                 not_valid_before=date(year=2000, month=1, day=1))
        avantage.conditions.set([condition_non_1a])

        adhesion = MembershipType.objects.create(name='VAdhésion',
                                                 description="La carte VAdhésion te permet d'adhérer à n'importe quelle "
                                                             "asso*, elle n'est à prendre qu'une fois. Les fonds sont "
                                                             "ensuite redistribués par le CVA aux assos porteuses de "
                                                             "projets, sous forme de subventions et deux fois par "
                                                             "an.\n*Certaines demanderont une cotisation supplémentaire. "
                                                             "N'hésites pas à te renseigner auprès d'elles !",
                                                 price_ht=4.72, price_ttc=5, enabled=True,
                                                 not_valid_after=date(year=2038, month=1, day=1),
                                                 not_valid_before=date(year=2000, month=1, day=1))
        adhesion.conditions.set([condition_non_1a])

        bizuths = MembershipType.objects.create(name='bizuths', description="VAdhésion & VAvantage primo-entrants 65€",
                                                price_ht=65,
                                                price_ttc=65, enabled=True,
                                                not_valid_after=date(year=2038, month=1, day=1),
                                                not_valid_before=date(year=2000, month=1, day=1))
        condition_bizuth = TypeCondition.objects.create(type=TypeCondition.YEAR_IS, value=1, negation=False,
                                                        description="bizuths")
        bizuths.conditions.set([condition_bizuth])

        py = Member.objects.create(first_name="Py", last_name="Chan", gender='W', email='pychan@yopmail.com',
                                   birthday=datetime.today(), phone='+336546546546', category='other')
        Membership.objects.create(member=py, membership=avantage, payment_method=1, valid=True)

        Membership.objects.create(member=Member.objects.create(
            first_name="Lúthien", last_name="Tinúviel", category='student', gender='W',
            email='luthien.tinuviel@yopmail.com',
            birthday=datetime(year=100, month=4, day=8),
            student_profile=StudentProfile.objects.create(
                study_year=doct, school=imld,
                department=arc, student_number='66546789787'
            )
        ), membership=avantage, payment_method=1, valid=True)

        Membership.objects.create(member=Member.objects.create(
            gender='M',
            first_name='Fëanor', last_name='Sílmarìl', email='feanor.silmaril@yopmail.com',
            category='other',
            birthday=datetime(year=20, month=9, day=25)
        ), membership=adhesion, payment_method=1, valid=True)

        Membership.objects.create(member=Member.objects.create(
            gender='M',
            first_name='Beren', last_name='Erchamion',
            category='other', email='beren.ercharmion@yopmail.com',
            birthday=datetime(year=300, month=4, day=13)
        ), membership=adhesion, payment_method=1, valid=True)

        Membership.objects.create(payment_method=2, valid=True, member=Member.objects.create(
            first_name="Hugotest", last_name="Thomastest", birthday=datetime.today(), gender='M', category='student',
            email='hugot.thomateest@yopmail.com',
            student_profile=StudentProfile.objects.create(
                study_year=a1, school=insa, department=pc, student_number='654654654'
            )
        ), membership=bizuths)

        Membership.objects.create(payment_method=3, valid=True, member=Member.objects.create(
            first_name="Enzotest", last_name="Zattarintest", birthday=datetime.today(), gender='M', category='student',
            email='enzo.zat@yopmail.com',
            student_profile=StudentProfile.objects.create(
                study_year=a2, school=insa, department=pc, student_number='654654654'
            )
        ), membership=avantage)
        Membership.objects.create(payment_method=2, valid=True, member=Member.objects.create(
            first_name="orentin", last_name="erthier", birthday=datetime.today(), gender='M', category='student',
            email='orentin.erthier@yopmail.com',
            student_profile=StudentProfile.objects.create(
                study_year=a3, school=insa, department=tc, student_number='2225987987'
            )
        ), membership=adhesion)

        Membership.objects.create(payment_method=2, valid=True, member=Member.objects.create(
            first_name="Thorin", last_name="Oakenshield", birthday=datetime.today(), gender='M', category='student',
            email='thorin.oakenshield@yopmail.com',
            student_profile=StudentProfile.objects.create(
                study_year=a5, school=kzdm, department=forge, student_number='12312313'
            )
        ), membership=adhesion)
        self.stdout.write(self.style.WARNING("Adding test user"))
        User = get_user_model()
        # from permissions.models import User
        selenium_user = User.objects.create(username='selenium')
        selenium_user.set_password('hjklhjkl')
        selenium_user.save()

        self.stdout.write(self.style.SUCCESS('Successfully added all seed data!'))
