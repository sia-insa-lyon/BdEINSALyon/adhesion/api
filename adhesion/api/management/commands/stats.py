from django.core.management.base import BaseCommand

from adhesion.api.models import *


class Command(BaseCommand):
    help = 'sort les statistiques des adhérents par année d\'étude'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        valid_memberships = Membership.objects.filter(
            valid=True,
            membership__not_valid_after__gte=timezone.now(),
            membership__not_valid_before__lte=timezone.now()
        )
        sp = StudentProfile.objects.filter(
            member__memberships__in=valid_memberships,
            school=StudySchool.objects.get(short_name='INSA')
        )

        print("1A", sp.filter(study_year__name='1A').count())
        print("2A", sp.filter(study_year__name='2A').count())
        print("3A", sp.filter(study_year__name='3A').count())
        print("4A", sp.filter(study_year__name='4A').count())
        print("5A", sp.filter(study_year__name='5A').count())
        print("Doctorants", sp.filter(study_year__name='Doctorant').count())

        print("autres", sp.exclude(study_year__name__in=['1A', '2A', '3A', '4A', '5A']).count())

        self.stdout.write(self.style.SUCCESS(f"total adhérents valides INSA : {sp.count()}"))
