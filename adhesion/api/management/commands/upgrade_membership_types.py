from django.core.management.base import BaseCommand
from django.utils.datetime_safe import date

from adhesion.api.models import MembershipType, TypeCondition


class Command(BaseCommand):
    help = 'Seed the database with the available membership types'

    def add_arguments(self, parser):
        # parser.add_argument('data_folder', type=str)
        pass

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Upgrading MembershipType by adding the light membership type...'))

        # Disabling the old normal 20€ card
        MembershipType.objects.filter(price_ttc=20).update(enabled=False)


        # Creating the condition for non-1A students
        condition_non_1a = TypeCondition.objects.get_or_create(type=TypeCondition.YEAR_IS, value=1, negation=True,
                                                     description="Non 1A")
        

        # Creating 5€ card
        adhesion = MembershipType.objects.create(name='VAdhésion',
                                             description="La carte VAdhésion te permet d'adhérer à n'importe quelle "
                                                         "asso*, elle n'est à prendre qu'une fois. Les fonds sont "
                                                         "ensuite redistribués par le CVA aux assos porteuses de "
                                                         "projets, sous forme de subventions et deux fois par "
                                                         "an.\n*Certaines demanderont une cotisation supplémentaire. "
                                                         "N'hésites pas à te renseigner auprès d'elles !",
                                             price_ht=4.72,
                                             price_ttc=5,
                                             enabled=True,
                                             not_valid_after=date(year=2038, month=1, day=1),
                                             not_valid_before=date(year=2000, month=1, day=1))
        adhesion.conditions.set([condition_non_1a])

        # Creating 20€ card
        avantage = MembershipType.objects.create(name='VAvantage',
                                            description="La carte VAvantages te permet d'accéder aux offres, services "
                                                        "et évènements du BdE ainsi qu'à des tarifs très avantageux "
                                                        "sur ceux de la Vie Associative.",
                                            price_ht=18.87,
                                            price_ttc=20.00,
                                            enabled=True,
                                            not_valid_after=date(year=2038, month=1, day=1),
                                            not_valid_before=date(year=2000, month=1, day=1))
        avantage.conditions.set([condition_non_1a])

        # Editing price for the bizuths card
        bizuths = MembershipType.objects.get(name='bizuths')
        if bizuths:
            bizuths.price_ht = 65
            bizuths.price_ttc = 65
            bizuths.description = "VAdhésion & VAvantage primo-entrants 65€"
            bizuths.save()

        self.stdout.write(self.style.SUCCESS('Successfully upgraded!'))
