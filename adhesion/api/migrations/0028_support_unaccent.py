# Generated by Django 3.1.13 on 2024-10-26 19:51

from django.db import migrations, models
from django.contrib.postgres.operations import CreateExtension

class Migration(migrations.Migration):

    dependencies = [
        ('api', '0027_auto_20201113_2122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='typecondition',
            name='type',
            field=models.CharField(choices=[('YEAR_IS', "L'année d'étude correspond à la valeur"), ('YEAR_LT', "L'année d'étude est inférieur à la valeur"), ('YEAR_GT', "L'année d'étude est supérieur à la valeur"), ('HAS_MEMBERSHIP', "A l'adhésion d'id donné")], max_length=30),
        ),
        CreateExtension('unaccent'),
    ]
