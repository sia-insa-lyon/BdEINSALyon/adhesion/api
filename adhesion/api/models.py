from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.datetime_safe import datetime
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField


class Member(models.Model):
    first_name = models.CharField(max_length=255, verbose_name=_('prénom'))
    last_name = models.CharField(max_length=255, verbose_name=_('nom'))
    gender = models.CharField(max_length=1, verbose_name=_('genre'), choices=(('M', _('Homme')), ('W', _('Femme'))))
    email = models.EmailField(verbose_name=_('email'), unique=True)
    phone = PhoneNumberField(verbose_name=_('téléphone'), blank=True, null=True)
    birthday = models.DateField(verbose_name=_('date de naissance'), blank=True, null=True)
    student_profile = models.OneToOneField('StudentProfile', on_delete=models.DO_NOTHING, blank=True, null=True,
                                           related_name='member')
    category = models.CharField(max_length=30, choices=(
        ('student', _('Etudiant')),
        ('worker', _('Employé Ecole')),
        ('other', _('Autre'))
    ), default='student')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    user = models.OneToOneField(to='permissions.User', null=True, blank=True, verbose_name=_('utilisateur'),
                                on_delete=models.CASCADE)
    va_cheque_received = models.BooleanField(verbose_name=_('chèque VA reçu'), default=False)

    @property
    def has_valid_membership(self):
        return self.memberships.filter(valid=True,
                                       membership__not_valid_after__gte=timezone.now(),
                                       membership__not_valid_before__lte=timezone.now()).count() > 0

    @property
    def has_valid_card(self):
        return self.cards.filter(activated=True).count() > 0

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)


class Card(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    activated = models.BooleanField(default=True)
    member = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='cards', null=True)
    code = models.CharField(max_length=255, blank=True, db_index=True)

    def __str__(self):
        return "#{} N° {} - {} {}".format(self.id, self.code, self.member.first_name, self.member.last_name)

    def deactivate(self):
        self.activated = False
        self.save()

    def activate(self, member: Member):
        self.member = member
        self.activated = True


class StudyYear(models.Model):
    """
    Store study years by a model because they can have different meaning than just the number.
    """

    name = models.CharField(max_length=25, verbose_name=_('nom'), unique=True)
    year = models.IntegerField(verbose_name=_('année'),
                               help_text=_('Cette valeur permet de comparer différente années entre elles'))
    order = models.IntegerField(verbose_name=_('ordre'),
                                help_text=_('Ordre de sélection, plus grand en premier'),
                                default=1)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('année')
        ordering = ['-order']


class StudySchool(models.Model):
    short_name = models.CharField(max_length=6, verbose_name=_('nom court'), unique=True)
    name = models.CharField(max_length=255, verbose_name=_('nom'), unique=True)

    def __str__(self):
        return self.short_name

    class Meta:
        verbose_name = _('école')
        ordering = ['short_name']


class StudyDepartment(models.Model):
    short_name = models.CharField(max_length=6, verbose_name=_('nom court'), unique=True)
    name = models.CharField(max_length=255, verbose_name=_('nom'), unique=True)
    school = models.ForeignKey(StudySchool, on_delete=models.PROTECT, blank=True, verbose_name=_('école'))
    study_years = models.ManyToManyField(StudyYear, verbose_name=_('années'))

    def __str__(self):
        return self.short_name

    class Meta:
        verbose_name = _('département')
        ordering = ['short_name']


class StudentProfile(models.Model):
    study_year = models.ForeignKey(StudyYear, on_delete=models.PROTECT, blank=True, null=True,
                                   verbose_name=_('année'), related_name='students')
    department = models.ForeignKey(StudyDepartment, on_delete=models.PROTECT, blank=True, null=True,
                                   verbose_name=_('département'), related_name='students')
    school = models.ForeignKey(StudySchool, on_delete=models.PROTECT, blank=True, null=True,
                               verbose_name=_('école'), related_name='students')
    student_number = models.CharField(max_length=20, blank=True, null=True, verbose_name=_(
        'numéro'))  # TODO: il fautdair enlever blank=True et mettre unique=True
    # TODO: il faudra aussi
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.student_number)

    class Meta:
        verbose_name = _('profile d\'étude')

    @staticmethod
    def from_api(data):
        if data is None:
            return None
        if 'pk' in data:
            profile = StudentProfile.objects.get_or_create(pk=data.pk)
        else:
            profile = StudentProfile()
        profile.school = data['school']
        profile.study_year = data['study_year']
        profile.department = data['department']
        profile.student_number = data['student_number'] if data['student_number'] != "" else None
        profile.save()
        return profile


class TypeCondition(models.Model):
    YEAR_IS = 'YEAR_IS'
    YEAR_LT = 'YEAR_LT'
    YEAR_GT = 'YEAR_GT'
    HAS_MEMBERSHIP = 'HAS_MEMBERSHIP'

    description = models.CharField(max_length=255, verbose_name=_('nom'), unique=True)
    type = models.CharField(
        max_length=30,
        choices=(
            (YEAR_IS, _('L\'année d\'étude correspond à la valeur')),
            (YEAR_LT, _('L\'année d\'étude est inférieur à la valeur')),
            (YEAR_GT, _('L\'année d\'étude est supérieur à la valeur')),
            (HAS_MEMBERSHIP, _('A l\'adhésion d\'id donné')),
        )
    )
    value = models.IntegerField(default=1, blank=True)
    negation = models.BooleanField(default=False)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = _('condition d\'adhésion')

    def valid(self, member):
        if self.negation:
            return not self._valid(member)
        else:
            return self._valid(member)

    def _valid(self, member):
        """
        Valid a condition for given member
        :param member:Member member to test for the condition
        :return: true if the condition is meet
        """
        if self.type in [TypeCondition.YEAR_IS, TypeCondition.YEAR_GT, TypeCondition.YEAR_LT, TypeCondition.HAS_MEMBERSHIP]:
            if member.student_profile_id is None:
                return False
            else:
                if self.type == TypeCondition.YEAR_IS:
                    return member.student_profile.study_year.year == self.value
                if self.type == TypeCondition.YEAR_LT:
                    return member.student_profile.study_year.year < self.value
                if self.type == TypeCondition.YEAR_GT:
                    return member.student_profile.study_year.year > self.value
                if self.type == TypeCondition.HAS_MEMBERSHIP:
                    return member.memberships.filter(membership_id=self.value, valid=True).exists()
        else:
            return False


class MembershipType(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('nom'), unique=True)
    description = models.TextField(verbose_name=_('description'), blank=True)
    price_ht = models.DecimalField(verbose_name=_('Prix HT'), decimal_places=2, max_digits=11)
    price_ttc = models.DecimalField(verbose_name=_('Prix TTC'), decimal_places=2, max_digits=11)
    enabled = models.BooleanField(default=False, verbose_name=_('activée'), help_text=_('peut être vendue'))
    not_valid_before = models.DateField(verbose_name=_('début'), help_text=_('valide à partir de cette date'))
    not_valid_after = models.DateField(verbose_name=_('fin'), help_text=_('invalide à partir de cette date'))
    conditions = models.ManyToManyField(TypeCondition, blank=True, related_name='membership_types')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('type d\'adhésion')
        ordering = ['name']

    def valid(self, member):
        """
        Valid if this membership type is valid for a given member
        :param member:Member member to test for the condition
        :return: true if the condition is meet
        """
        for condition in self.conditions.all():
            if not condition.valid(member):
                return False
        return self.enabled and self.not_valid_before < datetime.today().date() < self.not_valid_after


PAYMENT_ONLINE = 4


class Membership(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    member = models.ForeignKey(Member, on_delete=models.PROTECT, related_name='memberships')
    membership = models.ForeignKey(MembershipType, on_delete=models.PROTECT)
    valid = models.BooleanField(default=False)
    payment_method = models.IntegerField(verbose_name=_('Moyen de paiement'), choices=(
        (1, 'CB'),
        (2, 'Espèce'),
        (3, 'Chèque'),
        (PAYMENT_ONLINE, 'En ligne')
    ))
    created_by = models.TextField(verbose_name=_('Orga'), help_text="Nom de l'orga qui a créé l'adhésion",
                                  default="Non renseigné")

    class Meta:
        verbose_name = _('adhésion')

    def __str__(self):
        if self.valid:
            return "{} - {}".format(self.member, self.membership)
        else:
            return "(INVALIDE) {}-{}".format(self.member, self.membership)


@receiver(pre_save, sender=Membership)
def pre_save_handler(instance, *args, **kwargs):
    # Replacing membership if a valid membership of the same type already exist
    if instance._state.adding:
        Membership.objects.filter(member=instance.member, membership=instance.membership).delete()
    # If a user try to sneak the api by attributing a wrong membership to a user
    # Then we should raise an error
    if not instance.membership.valid(instance.member):
        raise Exception('Membership is not valid due to a condition')
