import os
import warnings
from typing import List

import jwt
import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.views import View
from jwt import DecodeError, ExpiredSignatureError
from rest_framework.authentication import get_authorization_header
from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.request import Request

from adhesion.api.models import Member


def get_keycloak_public_key() -> str:
    pk = os.environ.get('OIDC_RP_PK', None)
    if pk is not None:
        return pk
    else:
        r = requests.get(f"{settings.KEYCLOAK_URL}realms/{settings.REALM_NAME}/")
        pk = "-----BEGIN PUBLIC KEY-----\n" + r.json()['public_key'] + "\n-----END PUBLIC KEY-----"
        os.environ['OIDC_RP_PK'] = pk
        return pk


class HasMember(BasePermission):
    """
    Autorise uniquement les utilisateurs avec un objet adhérent
    """

    def has_permission(self, request, view):
        if request.user and request.user.is_authenticated:
            return request.user.member is not None
        return False


class KeycloakRoleMixin:
    required_roles = []  # autorise ces rôles
    read_only_roles = []  # autorise les méthodes GET, HEAD et OPTIONS à ces rôles

    def get_required_roles(self) -> List[str]:
        return self.required_roles

    def get_read_only_roles(self) -> List[str]:
        return self.read_only_roles

    def get_user_roles(self, request: Request):
        bearer_token = get_authorization_header(request)
        try:
            jwt_token = bearer_token.split(b' ')[1]
            infos = jwt.decode(jwt_token, get_keycloak_public_key(), verify=True, algorithms=('RS256',),
                               audience=settings.OIDC_RP_CLIENT_ID)
            user_roles = infos['resource_access'][settings.OIDC_RP_CLIENT_ID]['roles']  # filtre les rôles dans le JSON
            return user_roles
        except (KeyError, DecodeError, IndexError):
            return []


class KeycloakHasRolePermission(KeycloakRoleMixin, BasePermission):
    """
    Superclasse qui permet de définir des permissions basées sur des rôles Client keycloak
    attention: les rôles considérés sont seulement ceux du Client utilisé par le serveur Django
    Les rôles d'autres clients (comme ceux du frontend) ou du Realm entier ne sont pas comptés par cette classe
    De plus, il faut que l'audience du token Bearer JWT corresponde au nom du Client utilisé par Django (OIDC_RP_CLIENT_ID)
    voir https://stackoverflow.com/a/53627747/12643213 pour le fair si ça ne correspond pas
    """

    def has_permission(self, request: Request, view: View) -> bool:
        # return True #comment tester en dev :)
        user_roles = self.get_user_roles(request)
        required_roles = self.get_required_roles()
        permission = False
        permission = set(required_roles) & set(user_roles) == set(required_roles)
        return permission
        # vérifie que l'intersection de A et B
        # est A, donc que l'utilisateur a tous les rôles demandés


class KeycloakHasRolePermissionOrReadOnly(KeycloakHasRolePermission):
    def has_permission(self, request: Request, view: View) -> bool:
        # return True #comment tester en dev :)
        user_roles = self.get_user_roles(request)
        required_roles = self.get_required_roles()
        read_only_roles = self.get_read_only_roles()
        if request.method in SAFE_METHODS:
            user_r = set(user_roles) & set(required_roles)
            user_ro = set(user_roles) & set(read_only_roles)
            return user_r == set(required_roles) or user_ro == set(read_only_roles)
        else:
            return set(required_roles) & set(user_roles) == set(required_roles)  # vérifie que l'intersection de A et B
        # est A, donc que l'utilisateur a tous les rôles demandés


User = get_user_model()

from mozilla_django_oidc.contrib.drf import OIDCAuthentication


class KeycloakJWTAuthentication(OIDCAuthentication):
    def authenticate(self, request):
        """
        Authenticate the request and return a tuple of (user, token) or None
        if there was no authentication attempt.
        """
        access_token = self.get_access_token(request)

        if not access_token:
            return None
        try:
            infos = jwt.decode(access_token, get_keycloak_public_key(), verify=True, algorithms=('RS256',),
                               audience=settings.OIDC_RP_CLIENT_ID)
            user = None
            try:
                user = User.objects.get(id=infos['adhesion_user_id'])
            except User.DoesNotExist:
                user = Member.objects.get(id=infos['member_id']).user
            return user, access_token
        except Exception as e:
            if e is ExpiredSignatureError:
                warnings.warn(f"token expired {access_token}")
            elif e is DecodeError:
                warnings.warn(f"decodeError for token {access_token}")
            elif str(e) == "'adhesion_user_id'":
                warnings.warn("L'utilisateur Keycloak n'est pas relié à un utilisateur dans Adhésion")
            else:
                warnings.warn(f"exception dans l'auth JWT:{e}, {e.args}")
            warnings.warn("falling back to OIDCAuthentification")
            return super().authenticate(request)


class HasRoleLaverie(KeycloakHasRolePermission):
    required_roles = ['laverie']


class HasRoleVAChecker(KeycloakHasRolePermission):
    required_roles = ['vachecker']


class HasRoleTrezo(KeycloakHasRolePermission):
    required_roles = ['trezo']  # mettre 'trezo' ... plus tard


class HasRoleWei(KeycloakHasRolePermission):
    required_roles = ['wei']


class HasRoleStaff(KeycloakHasRolePermission):
    required_roles = ['staff']


class HasRoleStaffOrReadOnly(HasRoleStaff):
    def has_permission(self, request: Request, view: View) -> bool:
        return super().has_permission(request, view) or request.method in SAFE_METHODS


class HasRoleInte(KeycloakHasRolePermission):
    required_roles = ['inte']


class HasRoleCardsManager(KeycloakHasRolePermission):
    required_roles = ['cards_manager']
