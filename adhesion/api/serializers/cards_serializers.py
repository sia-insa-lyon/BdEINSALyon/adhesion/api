from rest_framework import serializers

from adhesion.api import models


class CardSerializer(serializers.ModelSerializer):
    valid_member = serializers.SlugRelatedField(slug_field='has_valid_membership', read_only=True, source='member')

    class Meta:
        model = models.Card
        fields = ('id', 'code', 'member', 'activated', 'valid_member')
