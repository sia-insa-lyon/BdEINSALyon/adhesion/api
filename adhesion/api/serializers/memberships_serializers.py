from rest_framework import serializers

from adhesion.api import models


class TypeConditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TypeCondition
        fields = ('id',
                  'description',
                  'type',
                  'value',
                  'negation')


class MembershipTypeSerializer(serializers.ModelSerializer):
    conditions = TypeConditionSerializer(many=True, read_only=True)
    price_ht = serializers.DecimalField(max_digits=5, decimal_places=2, localize=False, coerce_to_string=False)
    price_ttc = serializers.DecimalField(max_digits=5, decimal_places=2, localize=False, coerce_to_string=False)

    class Meta:
        model = models.MembershipType
        fields = ('id',
                  'name',
                  'description',
                  'price_ht',
                  'price_ttc',
                  'enabled',
                  'not_valid_before',
                  'not_valid_after',
                  'conditions')
        read_only = ('id',
                     'name',
                     'description',
                     'price_ht',
                     'price_ttc',
                     'enabled',
                     'not_valid_before',
                     'not_valid_after',
                     'conditions')

class TimeRangeSerializer(serializers.Serializer):
    start_datetime = serializers.DateTimeField()
    stop_datetime = serializers.DateTimeField()