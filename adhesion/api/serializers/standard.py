from rest_framework import serializers

from adhesion.api import models
from adhesion.api.serializers.cards_serializers import CardSerializer
from permissions.models import User


class StudentProfileSerializer(serializers.ModelSerializer):
    study_year = serializers.SlugRelatedField(slug_field='name', queryset=models.StudyYear.objects.all())
    school = serializers.SlugRelatedField(slug_field='short_name', queryset=models.StudySchool.objects.all())
    department = serializers.SlugRelatedField(slug_field='short_name', queryset=models.StudyDepartment.objects.all())

    class Meta:
        model = models.StudentProfile
        fields = (
            'id',
            'study_year', 'department', 'school', 'student_number')


# class CardSerializer(serializers.ModelSerializer):
#    class Meta:
#        model = models.Card
#        fields = '__all__'

class MembershipSerializer(serializers.ModelSerializer):
    membership = serializers.SlugRelatedField(slug_field='name', queryset=models.MembershipType.objects.all())

    class Meta:
        model = models.Membership
        # fields = ('id','member','membership','valid')
        fields = '__all__'


class MemberSerializer(serializers.ModelSerializer):
    """
    Requiert l'existence de student_profile, mettre à null si non applicable
    """
    student_profile = StudentProfileSerializer(allow_null=True)
    memberships = MembershipSerializer(many=True, read_only=True)
    cards = CardSerializer(many=True, allow_null=True, read_only=True)

    class Meta:
        model = models.Member
        fields = (
            'id',
            'first_name', 'last_name', 'email', 'phone',
            'gender', 'birthday', 'has_valid_membership', 'has_valid_card',
            'student_profile', 'category', 'memberships', 'cards',
            'va_cheque_received'
        )

    def create(self, validated_data):
        profile_data = validated_data.pop('student_profile')
        member = models.Member.objects.create(**validated_data)
        member.student_profile = models.StudentProfile.from_api(profile_data)
        member.save()
        validated_data.update(
            {'student_profile': profile_data})  # on remet ce qu'on a enlevé pour pas faire bugger les autres trucs
        return member

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('student_profile')
        instance.student_profile = models.StudentProfile.from_api(profile_data) # sera sauvegardé par super(ModelSerializer).update()

        return super().update(instance, validated_data)
    
    def to_internal_value(self, data):
        if 'student_profile' not in data:
            data['student_profile'] = None

        return super(MemberSerializer, self).to_internal_value(data)


class SimpleStudentProfileSerializer(serializers.ModelSerializer):
    study_year = serializers.SlugRelatedField(slug_field='name', queryset=models.StudyYear.objects.all())
    study_school = serializers.SlugRelatedField(slug_field='short_name', queryset=models.StudySchool.objects.all())
    study_department = serializers.SlugRelatedField(slug_field='short_name',
                                                    queryset=models.StudyDepartment.objects.all())

    class Meta:
        model = models.StudentProfile
        fields = ('study_year', 'department', 'school')


class SimpleMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Member
        fields = (
            'id',
            'first_name', 'last_name',
            'student_profile', 'category')


class UserSerializer(serializers.ModelSerializer):
    member = MemberSerializer(allow_null=True)
    card = CardSerializer(allow_null=True)

    class Meta:
        model = User
        exclude = ['password', '_is_staff', '_is_superuser']

    is_staff = serializers.BooleanField()
    is_superuser = serializers.BooleanField()

class ExternMemberSerializerCheckVA(serializers.ModelSerializer):
    class Meta:
        model = models.Member
        fields = (
            'first_name', 'last_name', 'has_valid_membership', 'has_valid_card'
        )


class ExternMemberSerializer24hPermanancier(serializers.ModelSerializer):
    class Meta:
        model = models.Member
        fields = (
            'first_name', 'last_name','email','has_valid_membership'
        )
