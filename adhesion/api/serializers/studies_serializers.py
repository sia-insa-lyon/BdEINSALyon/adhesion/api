from rest_framework import serializers

from adhesion.api import models


class StudyYearSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StudyYear
        fields = ('id', 'name', 'year')


class StudySchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StudySchool
        fields = ('id', 'name', 'short_name')


class StudyDepartmentSerializer(serializers.ModelSerializer):
    school = serializers.SlugRelatedField(slug_field='short_name', read_only=True)
    study_years = serializers.SlugRelatedField(slug_field='name', read_only=True, many=True)

    class Meta:
        model = models.StudyDepartment
        fields = ('id', 'name', 'short_name', 'school', 'study_years')
