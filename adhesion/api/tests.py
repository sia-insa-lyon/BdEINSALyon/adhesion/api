from django.test import TestCase
from rest_framework.test import APIRequestFactory

from adhesion.api.models import *


# Create your tests here.

class ModelsTest(TestCase):
    def init_db_departs(self):
        self.insa = StudySchool.objects.create(short_name='INSA', name='Institut national des sciences appliquées')
        self.cpe = StudySchool.objects.create(short_name='CPE', name='Chime Physique Éléctronique Lyon')

        self.a1 = StudyYear.objects.create(name='1A', year=1, order=1)
        self.a2 = StudyYear.objects.create(name='2A', year=2, order=2)
        self.a3 = StudyYear.objects.create(name='3A', year=3, order=3)
        self.a4 = StudyYear.objects.create(name='4A', year=4, order=4)
        self.a5 = StudyYear.objects.create(name='5A', year=5, order=5)
        self.doct = StudyYear.objects.create(name='Doctorant', year=6, order=6)

        self.tc = StudyDepartment.objects.create(short_name='TC', name='Télécom s&u', school=self.insa)
        self.tc.study_years.set([self.a3, self.a4, self.a5])

        self.pc = StudyDepartment.objects.create(short_name='FIMI', name='FIMI', school=self.insa)
        self.pc.study_years.set([self.a1, self.a2])

        self.gm = StudyDepartment.objects.create(short_name='GM', name='Génie de la mécanique', school=self.insa)
        self.gm.study_years.set([self.a3, self.a4, self.a5])

        self.ccppee = StudyDepartment.objects.create(short_name='CPE1', name='départ1', school=self.cpe)
        self.ccppee.study_years.set([self.a1, self.a2, self.a3, self.a4])

    def init_db_types_adhesions(self):
        self.normale = MembershipType.objects.create(name='normale', description="adhésion habituelle 20€",
                                                     price_ht=18.87,
                                                     price_ttc=20.00, enabled=True,
                                                     not_valid_after=datetime(year=2038, month=1, day=1),
                                                     not_valid_before=datetime(year=2000, month=1, day=1))
        self.condition_normale = TypeCondition.objects.create(type=TypeCondition.YEAR_IS, value=1, negation=True,
                                                              description="non-1A")
        self.normale.conditions.set([self.condition_normale])

        self.bizuths = MembershipType.objects.create(name='bizuths', description="adhésion primo-entrants 65€",
                                                     price_ht=65,
                                                     price_ttc=65, enabled=True,
                                                     not_valid_after=datetime(year=2038, month=1, day=1),
                                                     not_valid_before=datetime(year=2000, month=1, day=1))
        self.condition_bizuth = TypeCondition.objects.create(type=TypeCondition.YEAR_IS, value=1, negation=False,
                                                             description="bizuths")
        self.bizuths.conditions.set([self.condition_bizuth])

    def init_db_members(self):
        self.py_chan = Member.objects.create(first_name="Py", last_name="Chan", gender='W', email='pychan@yopmail.com',
                                             birthday=datetime.today(), phone='+336546546546', category='other')

        self.mini_biz = Member.objects.create(
            first_name="Mario",
            last_name="Bros",
            gender='M',
            email='mario@yopmail.com',
            birthday=datetime.today(),
            category='student',
            student_profile=StudentProfile.objects.create(
                study_year=self.a1, school=self.insa, department=self.pc, student_number='6546546788'
            )
        )

    # @classmethod
    # def setUpClass(cls) -> None:
    #    super(ModelsTest, cls).setUpClass()
    #    print("setup db...")
    #    cls.init_db_departs(cls)
    #    cls.init_db_types_adhesions(cls)
    #    cls.init_db_members(cls)

    def setUp(self) -> None:
        super(ModelsTest, self).setUp()
        self.init_db_departs()
        self.init_db_types_adhesions()
        self.init_db_members()
        self.request_factory = APIRequestFactory()
        # print(f"set up db, {Member.objects.count()} members")

    def test_get_members(self):
        self.failIf(Member.objects.all().count() == 0, "empty DB")

    def test_add_valid_membership(self):
        Membership.objects.create(member=self.py_chan, membership=self.normale, payment_method=1, valid=True)
        Membership.objects.create(member=self.mini_biz, membership=self.bizuths, payment_method=1, valid=True)

        self.assertTrue(self.py_chan.has_valid_membership, "no valid membership")

    def test_add_invalid_membership(self):
        with self.assertRaises(Exception):
            Membership.objects.create(member=self.mini_biz, membership=self.normale, payment_method=1, valid=True)

    def test_add_card(self):
        Membership.objects.create(member=self.py_chan, membership=self.normale, payment_method=1, valid=True)
        Card.objects.create(
            member=self.py_chan,
            code='c1232456598'
        )
        self.assertIsNotNone(self.py_chan.cards, "cannot get cards")

    def test_api_add_card(self):
        res = self.request_factory.post('/v1/cards', data={
            "code": "c8877887788",
            "member": self.mini_biz.id,
            "activated": True
        }, format='json')
        self.assertEqual(res.body, b'{"code":"c8877887788","member":2,"activated":true}')
        res2 = self.request_factory.get('/v1/cards/c8877887788/')
        print(res2.body)
