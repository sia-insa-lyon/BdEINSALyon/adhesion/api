from keycloak.exceptions import KeycloakError
from mozilla_django_oidc.auth import default_username_algo
from rest_framework.exceptions import APIException

from adhesion.api.models import Member
from customer.views import keycloak_admin

"""
Exemple d'un utilisateur Keycloak crée depuis Azure
{'id': 'dfae87ac-a2e2-44ec-a380-97f78ee5053b', 'createdTimestamp': 1599341757038, 'username': 'cdp', 'enabled': True, 'totp': False, 'emailVerified': False, 'firstName': 'Cdp', 'lastName': 'enculé', 'email': 'cdp@bde.fr', 'attributes': {'terms_and_conditions': ['1599341820']}, 'disableableCredentialTypes': [], 'requiredActions': [], 'notBefore': 0, 'access': {'manageGroupMembership': True, 'view': True, 'mapRoles': True, 'impersonate': True, 'manage': True}}

Exemple d'un bizuth crée par Adhésion suite à l'inscription en ligne
{'id': '584248f3-8c3b-43a5-afa2-9b2de9ac5977',
'createdTimestamp': 1590761498181,
'username': 'dv0de7sdlphjqri8nnvrlayyy4q',
'enabled': True, 'totp': False,
'emailVerified': True,
'firstName': 'Jean', 'lastName': 'Ribes',
'email': 'jean.christophe.ribes@sfr.fr', 'attributes':
 {'birthday': ['1998-01-22'], 'terms_and_conditions': ['1595953537'], 'gender': ['M'], 'school': ['INSA'], 'study_year': ['1A'], 'adhesionUserId': ['4'], 'category': ['student'], 'department': ['FIMI'], 'locale': ['fr'], 'memberId': ['9']},
 'disableableCredentialTypes': [],
 'requiredActions': [], 'notBefore': 0,
 'access': {'manageGroupMembership': True, 'view': True, 'mapRoles': True, 'impersonate': True, 'manage': True}}

"""


def format_kc_username(member: Member) -> str:
    return default_username_algo(member.email.encode('utf-8')).lower()


def get_kc_user(member: Member) -> dict:
    kc_username = format_kc_username(member)
    try:
        res = keycloak_admin.get_users(query={"search": kc_username})
        for u in res:
            if u['username'] == kc_username:
                return u
    except KeycloakError as e:
        raise e


def import_member_to_keycloak(member: Member):
    kc_username = format_kc_username(member)
    if keycloak_admin.get_user_id(kc_username) is not None:
        raise APIException(detail="Keycloak: Un utilisateur existe déjà avec cette adresse e-mail")
    kc_user_id = keycloak_admin.create_user({
        "email": member.email,
        "username": kc_username,
        "enabled": True,
        "firstName": member.first_name,
        "lastName": member.last_name,
        "realmRoles": [],
    })
    kc_attributes = {
        "birthday": member.birthday,
        "gender": member.gender,
        "memberId": member.id,
        "category": member.category
    }
    if member.user:
        kc_attributes["adhesionUserId"] = member.user.id,
    if member.category == 'student' and member.student_profile:
        kc_attributes['school'] = member.student_profile.school.short_name
        kc_attributes['study_year'] = member.student_profile.study_year.name
        kc_attributes['department'] = member.student_profile.department.short_name
        kc_attributes['student_number'] = member.student_profile.student_number
    keycloak_admin.update_user(user_id=kc_user_id, payload={
        'attributes': kc_attributes,
    })


def sync_member_to_keycloak(member: Member):
    kc_user = get_kc_user(member)
    kc_attributes = kc_user['attributes']

    kc_attributes.update({
        "birthday": member.birthday,
        "gender": member.gender,
        "memberId": member.id,
        "category": member.category
    })
    if member.user:
        kc_attributes["adhesionUserId"] = member.user.id,

    keycloak_admin.update_user(user_id=kc_user['id'], payload={
        "email": member.email,
        "firstName": member.first_name,
        "lastName": member.last_name,
        'attributes': kc_attributes,
    })
