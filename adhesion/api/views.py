import re
from random import randint

from dateutil.utils import today
from django.db.models import Count
from django.http import Http404, JsonResponse
from django.utils.datetime_safe import date
# Create your views here.
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters, status
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.exceptions import APIException, NotFound
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from adhesion.api.models import StudyYear, StudySchool, StudyDepartment, Membership, Card, MembershipType
from adhesion.api.pagination import CardsPagination, MembershipsPagination, MembersPagination
from adhesion.api.permissions import *
from adhesion.api.serializers import MemberSerializer, StudyYearSerializer, \
    StudySchoolSerializer, StudyDepartmentSerializer, CardSerializer, MembershipTypeSerializer
from adhesion.api.serializers.memberships_serializers import TimeRangeSerializer
from adhesion.api.serializers.standard import MembershipSerializer, UserSerializer
from permissions.models import User
from django.db.models import Func, Value
from django.db.models.functions import Lower


class MembersViewset(viewsets.ModelViewSet):
    permission_classes = [HasRoleInte]

    serializer_class = MemberSerializer
    pagination_class = MembersPagination
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('birthday', 'gender', 'student_profile__study_year')
    search_fields = ('$first_name', '$last_name', '^email', '=student_profile__student_number', '=cards__code')

    def get_queryset(self):

        if 'biz' in self.request.query_params.keys():
            if self.request.query_params['biz'] == 'true':
                return Member.objects.annotate(num_adhesions=Count('memberships')).filter(num_adhesions__lte=0)
        return Member.objects.all()


class CardsViewset(viewsets.ModelViewSet):
    permission_classes = [HasRoleInte]

    queryset = Card.objects.filter(activated=True)
    serializer_class = CardSerializer
    lookup_field = 'code'
    pagination_class = CardsPagination

    def get_object(self, *args, **kwargs) -> Card:
        cards = Card.objects.filter(code=self.kwargs['code'])
        if cards.count() > 0:
            return cards.first()
        else:
            raise Http404

    def get_queryset(self):

        if 'member' in self.request.query_params.keys():
            membre = Member.objects.all().get(id=int(self.request.query_params['member']))
            return Card.objects.filter(member=membre)

        return Card.objects.filter()

    def create(self, request, *args, **kwargs):
        """
        Au lieu de créer une carte VA, cela en récupère une existante, pour s'accorder au nouveau
        mode de fonctionnement des cartes VA dans la DB
        """
        serializer = CardSerializer(data=request.data)
        if serializer.is_valid():
            try:
                membre: Member = serializer.validated_data['member']
                card = Card.objects.get(code=serializer.validated_data['code'])

                if card.member is not None:  # empêche les doublons de scan !
                    raise APIException(detail="Cette carte VA est déjà attribuée !", code=status.HTTP_400_BAD_REQUEST)

                card.activate(member=membre)
                card.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            except Card.DoesNotExist:
                raise NotFound("Il n'y a pas de carte VA avec ce code !")
            except Member.DoesNotExist:
                raise NotFound()
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['get'])
    def member(self, request, pk=None, *args, **kwargs):
        return Response(MemberSerializer(self.get_object().member).data)

    def destroy(self, request, *args, **kwargs):
        """
        On ne supprime jamais les cartes VA sinon il y a risque d'avoir des doublons de codes VA
        """
        card = self.get_object()
        card.deactivate()
        return Response(CardSerializer(instance=card).data)


class CachingMixin:
    @method_decorator(cache_page(60 * 30))  # 30 minutes
    def list(self, *args, **kwargs):
        print('cached', self.__class__)
        return super(CachingMixin, self).list(*args, **kwargs)


class MembershipTypesViewset(viewsets.ModelViewSet):
    """
    Renvoie les différents types d'adhésion.
    Pour énumerer les types dispos pour un membre, ajouter ?member=id
    """
    queryset = MembershipType.objects.filter(enabled=True, not_valid_after__gte=date.today(),
                                             not_valid_before__lte=date.today())
    serializer_class = MembershipTypeSerializer
    lookup_field = 'name'
    permission_classes = [HasRoleStaffOrReadOnly]

    def get_queryset(self):
        queryset = MembershipType.objects.filter(enabled=True, not_valid_after__gte=date.today(),
                                                 not_valid_before__lte=date.today())
        if 'member' in self.request.query_params.keys():
            membre = Member.objects.all().get(id=int(self.request.query_params['member']))
            for membershiptype in MembershipType.objects.all():

                if not membershiptype.valid(membre):
                    queryset = queryset.exclude(id=membershiptype.id)
        return queryset


class MembershipViewset(viewsets.ModelViewSet):
    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer
    pagination_class = MembershipsPagination

    permission_classes = [HasRoleInte]

    def create(self, request, *args, **kwargs):
        serializer = MembershipSerializer(data=request.data)
        request.data['created_by'] = self.request.user.username
        if serializer.is_valid():
            membership = serializer.save()
            membership.save()
            return Response(MembershipSerializer(membership).data)
        raise APIException(serializer.errors)


class StudyYearViewset(CachingMixin, viewsets.ReadOnlyModelViewSet):
    queryset = StudyYear.objects.all()
    serializer_class = StudyYearSerializer
    permission_classes = [HasRoleStaffOrReadOnly]


class StudySchoolViewset(CachingMixin, viewsets.ReadOnlyModelViewSet):
    queryset = StudySchool.objects.all()
    serializer_class = StudySchoolSerializer
    permission_classes = [HasRoleStaffOrReadOnly]


class StudyDepartmentViewset(CachingMixin, viewsets.ReadOnlyModelViewSet):
    queryset = StudyDepartment.objects.all()
    serializer_class = StudyDepartmentSerializer
    permission_classes = [AllowAny]


class UserViewSet(RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    """
    Renvoie l'utilisateur effectuant la requête, avec ses données Adhésion
    NOTE: ça ne marche évidemment pas avec le mode "Client-credentials"
    """

    def get(self, request, **kwargs):
        serializer = None
        print(request.user)
        if request.user:
            return Response(UserSerializer(request.user).data)
        else:
            return Response("No user found", status=404)

    queryset = User.objects.all()


@permission_classes([HasRoleStaff])
@api_view(['GET'])
def export_va_member(request):
    exported_members = []
    for member in Member.objects.filter():
        if member.has_valid_membership:
            exported_members.append(member.email + ' ' + member.first_name + ' ' + member.last_name)
    return Response(exported_members)


class FeuilleCaisseView(RetrieveAPIView):
    permission_classes = [HasRoleTrezo]
    serializer_class = TimeRangeSerializer

    def query_response(self, queryset):
        types = []
        for memberhip_type in MembershipType.objects.order_by('enabled').order_by('not_valid_before').reverse():
            types.append({
                "type": MembershipTypeSerializer(memberhip_type).data,
                "total": queryset.filter(valid=True, membership=memberhip_type).count(),
                "cb": queryset.filter(valid=True, membership=memberhip_type, payment_method=1).count(),
                "espece": queryset.filter(valid=True, membership=memberhip_type, payment_method=2).count(),
                "cheque": queryset.filter(valid=True, membership=memberhip_type, payment_method=3).count(),
                "int": queryset.filter(valid=True, membership=memberhip_type, payment_method=4).count(),
            })
        return Response({
            "total": {
                "type": None,
                "count": queryset.count(),
                "cb": queryset.filter(valid=True, payment_method=1).count(),
                "espece": queryset.filter(valid=True, payment_method=2).count(),
                "cheque": queryset.filter(valid=True, payment_method=3).count(),
                "int": queryset.filter(valid=True, payment_method=4).count(),
            },
            "par_types": types
        })

    def get(self, request, **kwargs):
        queryset = Membership.objects.exclude(member=None).filter(created_at__gte=today().replace(minute=0, hour=0),
                                                                  created_at__lte=today().replace(minute=59, hour=23))
        return self.query_response(queryset)

    def post(self, request, **kwargs):
        trs = TimeRangeSerializer(data=request.data)
        trs.is_valid(raise_exception=True)
        print(trs.validated_data['start_datetime'])
        queryset = Membership.objects.exclude(member=None)
        queryset = queryset.filter(created_at__gte=trs.validated_data['start_datetime'],
                                   created_at__lte=trs.validated_data['stop_datetime'])
        # MembershipType.objects.filter(not_valid_before__lte=trs.validated_data['start_datetime'],
        # not_valid_after__gte=trs.validated_data['stop_datetime'])
        return self.query_response(queryset)


@permission_classes([HasRoleStaff])
def problemes_db(request):
    # [{'code': '', 'num': 2},....]
    doublons_cartes_va = Card.objects.values('code').annotate(num=Count('id')).filter(num__gt=1)
    return Response({
        'doublons_cartes_va': doublons_cartes_va,
    })


class InfosViewset(viewsets.ViewSet):  # c'est un viewset pour qu'il apparaisse dans l'explorateur
    """
    Permet de récupérer la configuration de l'application
    """
    permission_classes = [AllowAny]

    def list(self, request, *args, **kwargs):
        return Response({
            'places_wei': settings.PLACES_WEI,
            'date_wei': settings.DATE_WEI,
        })

# Replaces all accentuated characters by their non-accentuated equivalent
# Enable this feature in postgresql by running the following command:
# CREATE EXTENSION IF NOT EXISTS unaccent;
class Unaccent(Func):
    function = 'unaccent'
    template = '%(function)s(%(expressions)s)'

# Replaces all non-word characters by a dash
class Slugify(Func):
    function = 'regexp_replace'
    template = '%(function)s(%(expressions)s, \'[^\w]+\', \'-\', \'g\')'

# Removes the first letter of a string if it is 0
class RemoveFirst0(Func):
    function = 'regexp_replace'
    template = '%(function)s(%(expressions)s, \'^0\', \'\', \'g\')'

# Endpoint VAChecker
@api_view(['GET'])
@permission_classes([HasRoleVAChecker])
def va_check(request):
    """
    Get matching users memberships.
    :param request: GET Parameters: "first_name" + "last_name" (+ "birthday" + "slugify") OR "card_number" OR "student_number"
        first_name and last_name: Always case-insensitive. If one is defined, the other must be defined too.
        slugify: If defined, the search will be accentuated-letters-insensitive
        birthday format: YYYY-MM-DD (any separator is accepted)
        card_number: 12-digit card number, can start with a 'c', but the leading 'c' is not mandatory
        student_number: 8-digit student number, including the leading 0 is not mandatory
    :return: JsonResponse with the format :
            {
              "members": [
                {
                  "first_name": "John",
                  "last_name": "Doe",
                  "email": "john.doe@gmail.com",
                  "member_id": 1,
                  "memberships": [ # Only valid memberships
                    {
                      "name": "VAdhésion 2024-25"
                    },
                    {
                      "name": "VAvantage 2024-25"
                    }
                  ]
                }
              ]
            }
    """

    members = []

    if 'first_name' in request.query_params and 'last_name' in request.query_params:

        if 'slugify' in request.query_params:
            first_name_field = Slugify(Unaccent(Lower('first_name')))
            last_name_field = Slugify(Unaccent(Lower('last_name')))
            first_name_value = Slugify(Unaccent(Lower(Value(request.query_params['first_name']))))
            last_name_value = Slugify(Unaccent(Lower(Value(request.query_params['last_name']))))
        else:
            first_name_field = Lower('first_name')
            last_name_field = Lower('last_name')
            first_name_value = Lower(Value(request.query_params['first_name']))
            last_name_value = Lower(Value(request.query_params['last_name']))

        members = Member.objects.annotate(
            unaccented_first_name=first_name_field,
            unaccented_last_name=last_name_field
        ).filter(
            unaccented_first_name=first_name_value,
            unaccented_last_name=last_name_value
        )

        if 'birthday' in request.query_params:
            # Replace non-numeric characters bu a dash
            birthday_value = re.sub(r'\D+', '-', request.query_params['birthday'])
            members = members.filter(birthday=birthday_value)

    elif 'card_number' in request.query_params:
        card_number_value = "c" + re.sub(r'\D+', '', request.query_params['card_number'])
        cards = Card.objects.filter(code=card_number_value)
        for card in cards:
            members.append(card.member)

    elif 'student_number' in request.query_params:
        members = (Member.objects.annotate(
            student_number=RemoveFirst0('student_profile__student_number')
        )
        .filter(
            student_number=RemoveFirst0(Value(request.query_params['student_number']))
        ))

    else:
        return Response("Vous devez spécifier un paramètre card_number ou student_number ou first_name + last_name (et optionnellement birthday)",
                        status=status.HTTP_400_BAD_REQUEST)

    members_data = [
        {
            "first_name": member.first_name,
            "last_name": member.last_name,
            "email": member.email,
            "member_id": member.id,
            "memberships": [
                {
                    "name": membership.membership.name
                }
                for membership in member.memberships.filter(valid=True,
                                                            membership__not_valid_after__gte=date.today(),
                                                            membership__not_valid_before__lte=date.today(),
                                                            membership__enabled=True)
            ]
        } for member in members
    ]
    return JsonResponse({"members": members_data})


@api_view(['GET'])
@permission_classes([HasRoleCardsManager])
def generate_cards(request):
    """
    Génère de nouvelles cartes et renvoie les codes de ces dernières.
    """
    if 'num_cards' not in request.query_params or request.query_params['num_cards'] == '0':
        return Response("Vous devez spécifier un paramètre num_cards>0", status=status.HTTP_400_BAD_REQUEST)
    generated_codes = []
    generated_cards = []
    while len(generated_codes) != int(request.query_params['num_cards']):
        code = 'c' + str(randint(0, 999999999999)).zfill(12)
        if not Card.objects.filter(code=code).exists():
            generated_cards.append(Card(activated=False, member=None, code=code))
            generated_codes.append(code)
    Card.objects.bulk_create(generated_cards)
    return Response(generated_codes)
