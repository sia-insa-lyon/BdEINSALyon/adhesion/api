"""adhesion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from adhesion.api import views
from adhesion.api.views import export_va_member

router = routers.DefaultRouter()

router.register('cards', views.CardsViewset)
router.register('members', views.MembersViewset, 'members')
router.register('memberships', views.MembershipViewset)
router.register('membership_types', views.MembershipTypesViewset)
router.register('study_years', views.StudyYearViewset)
router.register('study_school', views.StudySchoolViewset)
router.register('study_department', views.StudyDepartmentViewset)
router.register('infos', views.InfosViewset, 'infos')
# router.register('me', views.UserViewSet, 'me')

urlpatterns = [
    path('v1/feuille_caisse/', views.FeuilleCaisseView.as_view()),
    path('v1/cards/generate_cards', views.generate_cards),
    path('v1/', include(router.urls)),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('me', views.UserViewSet.as_view()),
    path('va_check', views.va_check),
    path('swagger', get_swagger_view(title='Adhesion API')),
    path('customer/', include('customer.urls')),
    path('wei/', include('wei.urls')),
    path('export/', export_va_member),
    url(r'^oidc/', include('keycloak_oidc.urls')),
    path('mercanet/', include('mercanet.urls')),
    path('up2pay/', include('up2pay.urls')),
    path('laverie-api/', include('laverie.urls')),
    url(r'^$', TemplateView.as_view(template_name='home.html')),
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()

#print(get_kc_user(Member.objects.get(email='jean.christophe.ribes@sfr.fr')))
