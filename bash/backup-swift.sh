#!/usr/bin/env bash
set -e
FILENAME="adhesion-db-$(date +"%Y-%m-%d_%Hh%M").json"

echo "using database ${POSTGRES_DB}@${POSTGRES_HOST}"


/app/manage.py dumpdata --exclude contenttypes --exclude auth.permission --exclude sessions >  "$FILENAME"
swift --os-project-name "${OS_PROJECT_NAME}" -V=3 --os-auth-url "${OS_AUTH_URL}" -U "${OS_AUTH_USER}" -K "${OS_AUTH_KEY}" --os-storage-url "${OS_ENDPOINT}" upload "${OS_BUCKET_NAME}" --object-name "adhesion/$FILENAME" "$FILENAME"

echo "uploaded $FILENAME into ${OS_BUCKET_NAME}!"