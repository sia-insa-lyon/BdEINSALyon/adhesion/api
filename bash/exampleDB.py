from datetime import datetime, timedelta, time
import os
import django
from django.contrib.auth.hashers import make_password
from django.db import transaction
from django.forms import model_to_dict


def get_members():
    from adhesion.api.models import Member
    for member in Member.objects.all():
        print(model_to_dict(member))


@transaction.atomic
def insert_data():
    from adhesion.api.models import Member, Card, StudyDepartment, StudySchool, StudyYear, StudentProfile, Membership, MembershipType
    from permissions.models import User
    from wei.models import Bus, Bungalow, Bizuth

    User.objects.create(
        username=os.getenv('SUPERUSER_NAME', 'adhesion2'),
        email=os.getenv('SUPERUSER_EMAIL', 'test@example.com'),
        password=make_password(os.getenv('SUPERUSER_PASSWORD', 'password123')),
        is_superuser=True,
        is_staff=True,
        is_active=True
    )

    insa = StudySchool.objects.create(short_name='INSA', name='INSA de Lyon')

    a1 = StudyYear.objects.create(name='1A', year=1)
    a2 = StudyYear.objects.create(name='2A', year=2)

    pc = StudyDepartment.objects.create(short_name='PC', name='Premier Cycle', school=insa)
    pc.study_years.add(a1)
    pc.study_years.add(a2)
    pc.save()

    johndoe_profile = StudentProfile.objects.create(study_year=a2,
                                                    department=pc, school=insa,
                                                    student_number=123456789)

    johndoe_user = User.objects.create(
        username='jdoe',
        email='johndoe@yopmail.com',
        password=make_password('hjklhjkl'),
    )

    johndoe_member = Member.objects.create(first_name='John', last_name='Doe',
                                    email='john.doe@yopmail.com',
                                    gender='M', category='student',
                                    student_profile=johndoe_profile,
                                    user=johndoe_user)

    Card.objects.create(member=johndoe_member, code='c123456789')

    mtype = MembershipType.objects.create(name='achat', enabled=True,
                                          price_ht=10, price_ttc=11,
                                          description='lol G achte ma carte va',
                                          not_valid_before=(datetime.now() - timedelta(days=1)).date(),
                                          not_valid_after=(datetime.now() + timedelta(days=365)).date())

    Membership.objects.create(member=johndoe_member, valid=True, payment_method=3, membership=mtype)

    bus = Bus.objects.create(nom="Bambus", places=42)
    bungalow = Bungalow.objects.create(nom="LowLow", genre='M', places=100, bus=bus)
    Bizuth.objects.create(member=johndoe_member, bus=bus, bungalow=bungalow)


if __name__=='__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "adhesion.settings")
    django.setup()
    os.system("mv db.sqlite3 db.sqlite3.pybak")
    os.system("rm db.sqlite3")
    os.system("cp bak db.sqlite3")
    insert_data()
    get_members()