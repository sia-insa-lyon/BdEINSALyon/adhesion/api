#!/bin/bash
if [[ ${DATABASE_URL}="no" ]];then
        export DATABASE_URL="postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:5432/${DBNAME}"
fi
echo "using database url ${DATABASE_URL}"
$@