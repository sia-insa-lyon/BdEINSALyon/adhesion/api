#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

keycloak_ready() {
python3 << END
from os import getenv
from sys import exit
from keycloak import KeycloakOpenID
from keycloak.exceptions import KeycloakConnectionError

try:
    keycloak_openid = KeycloakOpenID(
        server_url=getenv('KEYCLOAK_URL'),
        client_id=getenv('OIDC_RP_CLIENT_ID'),
        realm_name=getenv('REALM_NAME'),
        client_secret_key=getenv('OIDC_RP_CLIENT_SECRET')
    )
    keycloak_openid.well_know()
except KeycloakConnectionError:
    exit(-1)
exit(0)
END
}

until keycloak_ready; do
  >&2 echo 'Waiting for Keycloak to become available...'
  sleep 1
done
>&2 echo 'Keycloak is available'

postgres_ready() {
python3 << END
import sys
from os import getenv

import psycopg2
from urllib.parse import urlparse

connection_info = urlparse(getenv('DATABASE_URL'))

try:
    psycopg2.connect(
        dbname=connection_info.path[1:],
        user=connection_info.username,
        password=connection_info.password,
        host=connection_info.hostname,
        port=connection_info.port,
    )
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)

END
}
until postgres_ready; do
  >&2 echo 'Waiting for PostgreSQL to become available...'
  sleep 1
done
>&2 echo 'PostgreSQL is available'

python3 manage.py collectstatic --noinput
python3 manage.py migrate --noinput
gunicorn adhesion.wsgi --bind=0.0.0.0:8000 --log-level=info --reload