#!/usr/bin/env bash
et -e
echo "exécution des migrations Django SQL"
yes yes | python3 manage.py migrate
echo "collectstatic: envoi des fichiers vers Minio/S3 si configuré"
python3 manage.py collectstatic --noinput
echo "lancement de Gunicorn"
gunicorn adhesion.wsgi -b 0.0.0.0:8000 --log-file -
