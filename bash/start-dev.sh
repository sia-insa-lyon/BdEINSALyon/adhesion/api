if [ -e venv ] && [ -d venv ]
then
    echo "Pas besoin de créer un virtualenv"
    . venv/bin/activate
else
    echo "création du virtualenv"
    virtualenv -p /usr/bin/python3 venv
    . venv/bin/activate
    pip install -r requirements.txt
fi
echo "lancement des migrations"
python3 manage.py migrate
if [ -e bak ]
then
    echo "la base de donnée existe et va être remplie"
else
    cp db.sqlite3 bak
fi
PYTHONPATH=$(pwd) python3 bash/exampleDB.py
echo "DEBUG=True" > .env