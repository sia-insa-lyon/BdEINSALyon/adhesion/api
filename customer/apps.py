from django.apps import AppConfig


class CustomerConfig(AppConfig):
    name = 'customer'
    verbose_name = "Pour l'authentification des utilisateurs"
