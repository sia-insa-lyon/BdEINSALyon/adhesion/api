import json
import logging

from django.utils.timezone import now

from mercanet.models import TransactionMercanet, STATUS_PAYED, STATUS_PAYMENT_FAILED
from up2pay.models import TransactionUp2Pay

logger = logging.getLogger('up2pay')


def update_customer_data_on_mercanet_payment_success(transaction: TransactionMercanet):
    if transaction.payed:
        member = transaction.issuer
        returnContext = json.loads(transaction.returnContext)

        membership = transaction.membership
        # except Bizuth.DoesNotExist:
        #    bizuth = None
        membership.valid = True
        membership.updated_at = now()
        membership.created_by = f"MercaNET, transaction #{transaction.transactionReference} le {transaction.transactionDateTime} (initié le {transaction.created_at})"
        membership.save()

        # if bizuth is not None:
        #    bizuth.a_paye = True
        #    bizuth.save()

        transaction.status = STATUS_PAYED
        transaction.save()
    else:
        transaction.status = STATUS_PAYMENT_FAILED
        transaction.save()


def update_customer_data_on_up2pay_payment_success(transaction: TransactionUp2Pay):
    if transaction.payed:
        member = transaction.issuer
        membership = transaction.membership

        membership.valid = True
        membership.updated_at = now()
        membership.created_by = f"Up2Pay, transaction #{transaction.transaction_reference} le {transaction.transaction_datetime} (initié le {transaction.created_at})"
        membership.save()


        logger.debug(f"Transaction {transaction.transaction_reference} succeeded with code {transaction.response_code}")
        transaction.status = STATUS_PAYED
        transaction.save()
    else:
        logger.debug(f"Transaction {transaction.transaction_reference} failed with code {transaction.response_code}")
        transaction.status = STATUS_PAYMENT_FAILED
        transaction.save()
