from rest_framework import serializers

from adhesion.api.models import Member
from adhesion.api.serializers import MembershipTypeSerializer
from adhesion.api.serializers.standard import MemberSerializer, MembershipSerializer, StudentProfileSerializer
from permissions.models import User
from wei.serializers import PublicBizuthSerializer


class UserSerializer(serializers.ModelSerializer):
    """
    Ne pas utiliser pour créer des Users
    """

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name']


class CustomerMemberSerializer(MemberSerializer):
    class Meta:
        model = Member
        exclude = ('created_at',)

    bizuth = PublicBizuthSerializer(read_only=True)
    has_valid_membership = serializers.BooleanField(read_only=True)


class RegistrationSerializer(MemberSerializer):
    class Meta:
        model = Member
        fields = (
            'first_name', 'last_name', 'email', 'phone',
            'gender', 'birthday',
            'student_profile', 'category', 'password')
        extra_kwargs = {
            'password': {'write_only': True}

        }

    student_profile = StudentProfileSerializer(allow_null=True, required=False, default=None)
    password = serializers.CharField()


class RegistrationUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name')

class JoinVASerializer(serializers.Serializer):
    payment_method = serializers.ChoiceField(choices=[
        (1, 'CB'),
        (2, 'Espèce'),
        (3, 'Chèque'),
        (4, 'En ligne')
    ])
    membership_type_id = serializers.IntegerField(required=False)


class CustomerMembershipSerializer(MembershipSerializer):
    membership = MembershipTypeSerializer()
