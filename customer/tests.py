import json
import sys
import unittest

from django.test import TestCase
from keycloak.exceptions import KeycloakError
from keycloak.keycloak_admin import KeycloakAdmin
# https://python-keycloak.readthedocs.io/en/latest/
from mozilla_django_oidc.auth import default_username_algo
from rest_framework.test import APIClient

"""
Tous ces tests nécessitent un serveur keycloak !
Il faut un Client sur keycloak avec des permissions d'administration comme l'export suivant
{
    "clientId": "adhesion-api-dev",
    "rootUrl": "https://neon.home.ribes.ovh/",
    "adminUrl": "http://neon.home.ribes.ovh/",
    "baseUrl": "http://neon.home.ribes.ovh/oidc/authenticate",
    "surrogateAuthRequired": false,
    "enabled": true,
    "alwaysDisplayInConsole": false,
    "clientAuthenticatorType": "client-secret",
    "redirectUris": [
        "http://localhost:8080/*",
        "http://neon.home.ribes.ovh/*",
        "https://neon.home.ribes.ovh/*"
    ],
    "webOrigins": [
        "http://localhost:3000",
        "+",
        "http://neon.home.ribes.ovh"
    ],
    "notBefore": 0,
    "bearerOnly": false,
    "consentRequired": false,
    "standardFlowEnabled": true,
    "implicitFlowEnabled": false,
    "directAccessGrantsEnabled": false,
    "serviceAccountsEnabled": true,
    "publicClient": false,
    "frontchannelLogout": false,
    "protocol": "openid-connect",
    "attributes": {
        "saml.assertion.signature": "false",
        "saml.multivalued.roles": "false",
        "saml.force.post.binding": "false",
        "saml.encrypt": "false",
        "saml.server.signature": "false",
        "saml.server.signature.keyinfo.ext": "false",
        "exclude.session.state.from.auth.response": "false",
        "saml_force_name_id_format": "false",
        "saml.client.signature": "false",
        "tls.client.certificate.bound.access.tokens": "false",
        "saml.authnstatement": "false",
        "display.on.consent.screen": "false",
        "saml.onetimeuse.condition": "false"
    },
    "authenticationFlowBindingOverrides": {},
    "fullScopeAllowed": true,
    "nodeReRegistrationTimeout": -1,
    "protocolMappers": [
        {
            "name": "Client IP Address",
            "protocol": "openid-connect",
            "protocolMapper": "oidc-usersessionmodel-note-mapper",
            "consentRequired": false,
            "config": {
                "user.session.note": "clientAddress",
                "id.token.claim": "true",
                "access.token.claim": "true",
                "claim.name": "clientAddress",
                "jsonType.label": "String"
            }
        },
        {
            "name": "Client Host",
            "protocol": "openid-connect",
            "protocolMapper": "oidc-usersessionmodel-note-mapper",
            "consentRequired": false,
            "config": {
                "user.session.note": "clientHost",
                "id.token.claim": "true",
                "access.token.claim": "true",
                "claim.name": "clientHost",
                "jsonType.label": "String"
            }
        },
        {
            "name": "Client ID",
            "protocol": "openid-connect",
            "protocolMapper": "oidc-usersessionmodel-note-mapper",
            "consentRequired": false,
            "config": {
                "user.session.note": "clientId",
                "id.token.claim": "true",
                "access.token.claim": "true",
                "claim.name": "clientId",
                "jsonType.label": "String"
            }
        }
    ],
    "defaultClientScopes": [
        "web-origins",
        "role_list",
        "profile",
        "roles",
        "email"
    ],
    "optionalClientScopes": [
        "address",
        "phone",
        "offline_access",
        "microprofile-jwt"
    ],
    "access": {
        "view": true,
        "configure": true,
        "manage": true
    }
}
"""


class KeycloakAdmin2APItest(unittest.TestCase):
    def setUp(self) -> None:
        self.keycloak_admin = KeycloakAdmin(
            server_url="https://sso.asso-insa-lyon.fr/auth/",
            client_id='adhesion-api-dev',
            realm_name='asso-insa-lyon',
            client_secret_key="f15cb0c1-01db-4aa9-be0e-bbffd80104d7",
            verify=True)

    def test_count_users(self):
        print(f" users: {self.keycloak_admin.users_count()}")

    def test_list_users(self):
        print(f" users: {self.keycloak_admin.get_users({})}")

    def test_delete_user(self):
        return ("si on le supprime alors on peut pas vérifier les mails ...")
        self.get_user()
        self.keycloak_admin.delete_user(user_id=self.new_user_id)

    def test_create_user(self):
        ret = self.keycloak_admin.create_user(  # si l'username existe déjà rien n'est fait
            {
                "email": "luthien.tinuviel@yopmail.com",
                "username": "usernamehere",
                "enabled": True,
                "firstName": "Lúthien",
                "lastName": "Tinúviel",
                # Password Policy not met si le mot de passe est trop faible
                "realmRoles": [],
                "attributes": {"birthday": "1970-01-01",
                               "gender": "W"}
            })
        print(f"Return : {ret}")

    def get_user(self):
        self.new_user_id = self.keycloak_admin.get_user_id("usernamehere")
        print(f"userid: {self.new_user_id}")
        return self.new_user_id

    def test_attr(self):
        response = self.keycloak_admin.update_user(user_id=self.get_user(),
                                                   payload={'attributes': {  # ça efface les autres attributs
                                                       'category': 'student',
                                                   },
                                                       'firstName': 'Beren'})

    def test_update_account(self):
        # CONFIGURE_TOTP, terms_and_conditions, UPDATE_PASSWORD, UPDATE_PROFILE, VERIFY_EMAIL, update_user_locale
        # lol c'est même pas dans la doc Rest KC
        return ("pas à chaque fois sinon ")
        self.get_user()
        self.keycloak_admin.send_update_account(user_id=self.new_user_id,
                                                payload=json.dumps(['UPDATE_PASSWORD', 'VERIFY_EMAIL']))

    def test_verify_email(self):
        return ("En fait il vaut mieux aussi modifier le mot de passe ...")
        self.keycloak_admin.send_verify_email(user_id=self.get_user())


from adhesion.api.management.commands.seeddb import Command


class CustomerTest(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()
        self.keycloak_admin = KeycloakAdmin(
            server_url="https://sso.asso-insa-lyon.fr/auth/",
            client_id='adhesion-api-dev',
            realm_name='asso-insa-lyon',
            client_secret_key="f15cb0c1-01db-4aa9-be0e-bbffd80104d7",
            verify=True)
        print("Seeding database", file=sys.stderr)
        Command().handle()

    def test_registration(self):
        try:
            self.keycloak_admin.delete_user(
                self.keycloak_admin.get_user_id(default_username_algo("ar.pharazon@yopmail.com").lower()))
        except KeycloakError:
            print("user not deleted")
        response = self.client.post(path='/customer/registration/', data={
            "first_name": "Ar",
            "last_name": "Phârazon",
            "birthday": "1970-12-30",
            "email": "ar.pharazon@yopmail.com",
            "gender": "M",
            "phone": "+33123456777",
            "category": "student",
            "password": "Hjklhjkl2",
            "student_profile": {
                "study_year": "1A",
                "school": "INSA",
                "department": "FIMI",
                "student_number": "5556546546545"
            },
        }, format='json')
        print(f"response ({response.status_code}): {response.data}")

        self.assertEqual(response.status_code, 201)

        self.client.login(
            username=default_username_algo("ar.pharazon@yopmail.com").lower(),
            password='Hjklhjkl2'
        )
        self.assertEqual(self.client.get(path='/me').status_code, 200)

if __name__ == "__main__":
    KeycloakAdmin2APItest()
