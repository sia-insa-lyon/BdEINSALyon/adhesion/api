from rest_framework.routers import DefaultRouter

from customer import views

router = DefaultRouter()
router.register('member', views.MemberViewSet, 'member')
router.register('joinva', views.JoinVaViewset, 'joinva')
router.register('registration', views.RegistrationViewSet, 'registration')
urlpatterns = [
              ] + router.urls
