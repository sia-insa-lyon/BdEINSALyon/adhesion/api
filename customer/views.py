import datetime
import logging

from django.conf import settings
from django.db.models import Q
from django.utils import timezone
from django.utils.datetime_safe import datetime as safedatetime
from keycloak import KeycloakAdmin
from keycloak.exceptions import KeycloakGetError, KeycloakAuthenticationError
from mozilla_django_oidc.auth import default_username_algo
from rest_framework import viewsets, status


from rest_framework.exceptions import APIException, ValidationError
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.request import Request
from rest_framework.response import Response

from adhesion.api.models import MembershipType, Membership, PAYMENT_ONLINE
from customer.serializers import *

payment_methods = {
    "online": [
            {
                "id": PAYMENT_ONLINE,
                "name": "Paiement en ligne (CA Up2Pay)"
            }
        ],
        "physical": [
            {"id": 3, "name": "Chèque"},
            {"id": 2, "name": "Espèces"},
            {"id": 1, "name": "Carte bancaire"},
        ]
    }

# User = get_user_model()
keycloak_admin: KeycloakAdmin
if settings.DISABLE_KEYCLOAK_CONN:
    keycloak_admin = None
    print("Keycloak-admin initialization disabled")
else:
    keycloak_admin = KeycloakAdmin(  # demande un token au lancement
        server_url=settings.KEYCLOAK_URL,
        client_id=settings.OIDC_RP_CLIENT_ID,
        realm_name=settings.REALM_NAME,
        client_secret_key=settings.OIDC_RP_CLIENT_SECRET,
        verify=True,
        auto_refresh_token=['get', 'post', 'put', 'delete']
    )

logger = logging.getLogger('customer')

def choose_membership_type(member: Member) -> MembershipType:
    chosen_price = 9999999  # lol c Dijkstra
    chosen_type = None
    for membershiptype in MembershipType.objects.filter(
            enabled=True,
            not_valid_after__gte=safedatetime.today(),
            not_valid_before__lte=safedatetime.today(),
    ):
        if membershiptype.valid(member) and membershiptype.price_ttc < chosen_price:
            chosen_type = membershiptype
            chosen_price = membershiptype.price_ttc

    return chosen_type


class RegistrationViewSet(viewsets.ViewSet):
    """
    Créee 3 objets dans 2 DBs...
    Sert à la toute première inscription pour que cela leur crée un compte sur Keycloak
    Si une personne s'inscrit avec un compte déjà existant dans adhésion, mais pas dans keycloak,
    on crée un compte keycloak et on actualise les infos
    """
    permission_classes = [AllowAny]

    def create(self, request):
        to_log_data = request.data.copy()
        to_log_data['password'] = '********'
        logger.debug(f"registration data: {to_log_data}")
        # Si l'utilisateur existe déjà dans Django, on actualise ses infos

        member = Member.objects.filter(email=self.request.data['email']).first()
        serialized_data = RegistrationSerializer(data=self.request.data, instance=member)
        django_user_data = RegistrationUserSerializer(data=self.request.data)

        if member is not None:
            logger.debug(f"member found: {member}")

        if serialized_data.is_valid(raise_exception=True) and django_user_data.is_valid(raise_exception=True):
            registration_data = serialized_data.validated_data
            student_profile = registration_data.get('student_profile', None)
            logger.debug(f"registration valid data: {registration_data}")

            try:
                new_username = default_username_algo(registration_data['email'].encode('utf-8')).lower()
                new_password = registration_data.pop('password')
                redirect_uri_emailverified = settings.PUBLIC_FRONTEND_URL  # l'url sur laquelle le mec est redirigé après vérification
                # Si l'utilisateur existe déjà dans Keycloak, on renvoie une erreur
                try:
                    if keycloak_admin.get_user_id(new_username) is not None:
                        # l'utilisateur existe déjà, erreur 400
                        raise ValidationError(detail="Un utilisateur avec la même adresse email existe déjà. Essayez de "
                                                  "vous connecter.")
                except (KeycloakAuthenticationError, KeycloakGetError):  # refresh du token
                    # si l'auto-refresh échoue, KeycloakGetError: 400: b'{"error":"invalid_grant","error_description":"Token is not active"}'
                    keycloak_admin.get_token()

                # création compte sur keycloak
                user_id = keycloak_admin.create_user(  # si l'username existe déjà rien n'est fait
                    {"email": registration_data['email'],
                     "username": new_username,
                     "enabled": True,
                     "firstName": registration_data['first_name'],
                     "lastName": registration_data['last_name'],
                     "realmRoles": [],  # remplir avec les rôles 'guest' (à cette étape les gens n'ont pas payé)
                     "credentials": [{"value": new_password, "type": "password", }]
                     }
                )

                # USER CREATION
                user = User.objects.filter(Q(username=new_username) | Q(email=registration_data['email'])).first()
                if user is not None:
                    logger.debug(f"User found: {user}")
                else:
                    user = django_user_data.save(username=new_username)
                user.set_password(new_password)
                user.save()

                # MEMBER CREATION
                if member is not None:
                    serialized_data.update(member, registration_data)
                else:
                    member = MemberSerializer().create(registration_data)

                member.user = user
                member.save()

                logger.debug(f"registration valid data: {registration_data}")

                # enrichissement d
                kc_attributes = {
                    "birthday": str(registration_data['birthday']),
                    "gender": registration_data['gender'],
                    "memberId": member.id,
                    "adhesionUserId": user.id,
                    "category": registration_data.get('category', 'other')
                }
                if registration_data.get('category', 'other') == 'student':
                    kc_attributes['school'] = student_profile['school'].short_name
                    kc_attributes['study_year'] = student_profile['study_year'].name
                    kc_attributes['department'] = student_profile['department'].short_name
                    kc_attributes['student_number'] = student_profile['student_number']
                    if student_profile['study_year'].name == '1A' and \
                            student_profile['school'].short_name == 'INSA':
                        redirect_uri_emailverified = f"{settings.PUBLIC_FRONTEND_URL}/authenticated/"

                # on enregistre les attributs
                keycloak_admin.update_user(user_id=user_id, payload={
                    'attributes': kc_attributes,
                })
                # envoi du mail de vérif
                try:
                    keycloak_admin.send_verify_email(client_id=settings.OIDC_RP_CLIENT_ID, user_id=user_id,
                                                     redirect_uri=redirect_uri_emailverified)
                except KeycloakGetError as e:
                    if not settings.DEBUG:
                        import sys
                        sys.stderr.write("redirect uri: " + redirect_uri_emailverified)
                        raise APIException(detail=e.error_message, code=e.response_code)
                return Response(data={
                    'member': MemberSerializer(instance=member).data,
                    'username': new_username,
                }, status=status.HTTP_201_CREATED)
            except ValueError as err:  # (KeycloakError, KeycloakGetError) as err:
                raise APIException(detail=f"Keycloak : {err.error_message}")


class MemberViewSet(viewsets.ModelViewSet):
    """
    Permet à un utilisateur de créer et modifier son profil
    """
    serializer_class = CustomerMemberSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Member.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        return serializer.save(user=self.request.user, updated_at=timezone.now())

    def update(self, request: Request, *args, **kwargs) -> Response:
        member = self.request.user.member
        if member.has_valid_membership:
            # on ne permet pas la modification quand les gens ont payé
            return Response(self.get_serializer(member).data)

        return super().update(request, *args, **kwargs)

    def perform_update(self, serializer):
        return serializer.save(user=self.request.user, updated_at=timezone.now())


class JoinVaViewset(viewsets.ViewSet):
    """
    Le client effectue une demande d'adhésion ici. Il spécifie s'il veut payer en ligne ou sur place
    Si paiement sur place, le choix du mode de paiement n'est pas important et devra pouvorir être changé par les orgas.

    L'adhésion sera validée en ligne après paiement & réponse MercaNET ou sur place par les orgas
    """
    permission_classes = [IsAuthenticated]

    def list(self, request):

        available_memberships = []
        for membershiptype in MembershipType.objects.filter(
                enabled=True,
                not_valid_after__gte=safedatetime.today(),
                not_valid_before__lte=safedatetime.today(),
        ):
            if membershiptype.valid(request.user.member):
                available_memberships.append(MembershipTypeSerializer(membershiptype).data)

        return Response(status=200, data={
            "payment_methods": payment_methods,
            "available_memberships": available_memberships
        })

    def create(self, request, format=None):
        try:
            member = request.user.member

            validated_serializer_data = self.get_vaserialiser()
            payment_method = validated_serializer_data['payment_method']

            if 'membership_type_id' in validated_serializer_data:
                chosen_type = MembershipType.objects.filter(id=validated_serializer_data['membership_type_id'])
                if chosen_type.count() == 0:
                    raise APIException(code=status.HTTP_400_BAD_REQUEST, detail="Type d'adhésion invalide.")
                return self.handle_new_membership(member, chosen_type.first(), payment_method)
            else:
                return self.handle_new_membership_auto(member, payment_method)

        except Member.DoesNotExist:
            raise APIException(code=status.HTTP_400_BAD_REQUEST,
                               detail="Vous devez d'abord remplir la fiche adhérent avant de passer à l'adhésion")

    def handle_new_membership_auto(self, member: Member, payment_method: int) -> Response:
        # Selects the membership that has the lowest price and is valid for the member
        chosen_type = choose_membership_type(member)
        if chosen_type is None:
            raise APIException(code=status.HTTP_424_FAILED_DEPENDENCY,
                               detail="Vous n'êtes pas autorisé à adhérer (c'est une erreur du serveur, pas de type "
                                      "d'adhésion trouvée. Source=mauvaise configuration des types d'adhésion)")
        return self.handle_new_membership(member, chosen_type, payment_method)

    def handle_new_membership(self, member: Member, membership_type: MembershipType, payment_method: int) -> Response:

        if (membership_type is None or not membership_type.enabled or not membership_type.valid(member)
                or not membership_type.not_valid_before <= datetime.date.today() <= membership_type.not_valid_after):
            raise APIException(code=status.HTTP_424_FAILED_DEPENDENCY, detail="Type d'adhésion invalide.")

        # Check if the user already has this membership
        valid_memberships = Membership.objects.filter(member=member, membership=membership_type, valid=True)
        if valid_memberships.count() > 0:
            raise APIException(code=status.HTTP_406_NOT_ACCEPTABLE,
                               detail="Vous avez déjà cette adhésion pour cette année")

        current_unpayed = Membership.objects.filter(
            member=member, valid=False, membership=membership_type
        )
        if current_unpayed.count() > 0:
            membership = current_unpayed.last()
            membership.payment_method = payment_method
            membership.save()

        else:
            membership = Membership(membership=membership_type, valid=False, created_by="API utilisateur, non payé",
                                    member=member, payment_method=payment_method)
            membership.save()

        return Response(status=201, data=CustomerMembershipSerializer(membership).data)

    def get_vaserialiser(self) -> dict:
        ser = JoinVASerializer(data=self.request.data)
        ser.is_valid(raise_exception=True)
        return ser.validated_data
