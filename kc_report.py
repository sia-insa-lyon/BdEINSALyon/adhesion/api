"""
Permissions nécessaires:
* view-users
* view-clients
"""
from os import getenv
from typing import *

import django
import dotenv
from django.conf import settings
from django.template import Template, Context
from keycloak import KeycloakAdmin

settings.configure(TEMPLATES=[{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
}])
django.setup()

template = """
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Rapport sur les permission Keycloak</title>
    <meta charset="utf-8"/>
</head>
<style>
    h3 {
        display: inline;
    }

    summary > h5 {
        display: inline;
    }

    li > details {

    }

    h4 {
        margin: 0.1rem;
        margin-left: 1rem;
    }

    body {
        margin: auto;
        max-width: 70vw;
    }
</style>
<body>
<h1>Rapport sur le contrôle d'accès du SSO associatif/BdE, Keycloak</h1>

<ul>
    {% for group in groups %}
        <li>
            <h3>{{ group.name }}</h3> <code>{{ group.path }}</code>
            <h4>Rôles</h4>
            <ul>
                {% for role in group.roles %}
                    <li>
                        <details>
                            <summary><h5>{{ role.name }}</h5></summary>
                            {{ role.description }}
                        </details>
                    </li>
                {% endfor %}
            </ul>
        <h4>Utilisateurs</h4>
        <ul>
            {% for user in group.users %}
                <li>
                {{ user.name }} &nbsp; &nbsp; <i> {{ user.email }}</i> &nbsp; <code></code>
                </li>
            {% endfor %}
        </ul>
        </li>
    {% endfor %}
</ul>
</body>
</html>
"""  # open('templates/kcreport.html').read()


def recurse_groups(kc: KeycloakAdmin, subgroups: List[dict], groups_out: List[dict]):
    for group in subgroups:
        groups_out.append(kc.get_group(group['id']))
        recurse_groups(kc, group.get('subGroups', []), groups_out)


if __name__ == '__main__':
    dotenv.read_dotenv()
    CLIENT_ID = getenv('OIDC_RP_CLIENT_ID', 'adhesion-api')
    ADHESION_CLIENT_ID = getenv('ANALYZE_CLIENT_ID', CLIENT_ID)
    keycloak_admin = KeycloakAdmin(  # demande un token au lancement
        server_url=getenv('KEYCLOAK_URL'),
        client_id=CLIENT_ID,
        realm_name='asso-insa-lyon',
        client_secret_key=getenv('OIDC_RP_CLIENT_SECRET'),
        verify=True,
        auto_refresh_token=['get', 'post', 'put', 'delete']
    )

    client_oid = keycloak_admin.get_client_id(ADHESION_CLIENT_ID)
    _roles = keycloak_admin.get_client_roles(client_oid)
    roles = {r['name']: r for r in _roles}

    groups = []
    recurse_groups(keycloak_admin, keycloak_admin.get_groups(), groups)

    out = []  # [{'name':,'path':,'roles':[{'name', 'description'}],'users':[{'nom+prenom':, 'mail':}]}}
    for group in groups:
        obj = {'name': group['name'], 'path': group['path'], 'roles': [], 'users': []}
        for client, client_roles in group['clientRoles'].items():
            if client == ADHESION_CLIENT_ID:
                for rolename in client_roles:
                    role = roles[rolename]
                    obj['roles'].append({'name': rolename, 'description': role['description']})
                for user in keycloak_admin.get_group_members(group['id']):
                    obj['users'].append({
                        'name': user['firstName'] + ' ' + user['lastName'],
                        'email': user['email'],
                    })
                print(user)
                out.append(obj)
                print(group)

    print(out)
    s = Template(template).render(Context({'groups': out}))
    with open('rapport.html', 'w+') as f:
        f.write(s)
