from typing import *

from keycloak import KeycloakAdmin

from adhesion import settings


def recurse_attrs(kc: KeycloakAdmin, groups: Iterator[dict]) -> List[Tuple[dict, List[str]]]:
    l = []
    for groupid in groups:
        group = kc.get_group(groupid['id'])
        try:
            l.append((groupid, group['attributes']['azure-groups']))
        except:
            pass
        l.extend(recurse_attrs(kc, group['subGroups']))
    return l


keycloak_admin = KeycloakAdmin(  # demande un token au lancement
    server_url='http://localhost:8080/auth/',
    client_id=settings.OIDC_RP_CLIENT_ID,
    realm_name=settings.REALM_NAME,
    client_secret_key='abba05b8-1aac-4c1d-acdb-8096dbd6ae02',
    verify=True,
    auto_refresh_token=['get', 'post', 'put', 'delete']
)


def update_user(kc: KeycloakAdmin, user: dict, groups_pairs: List[Tuple[dict, List[str]]]):
    """
    Synchronise les groupes Keycloak de l'utilisateur depuis ses groupes AzureAD (sharepoint)
    """
    user_azure_groups = set(user['attributes'].get('azure-groups', []))
    user_kc_groups = list(map(
        lambda g: g['id'],
        kc.get_user_groups(user['id'])
    ))
    for kc_group, azure_groups in groups_pairs:
        if len(set(azure_groups) & user_azure_groups) > 0 and kc_group['id'] not in user_kc_groups:
            # si l'utilisateur appartient à un des groupes azure requis
            kc.group_user_add(user_id=user['id'], group_id=kc_group['id'])


if __name__ == '__main__':
    user = keycloak_admin.get_user('86e6521b-44a4-4f93-8bf9-f8be26e17ff2')
    # print(user['attributes']['azure-groups'])
    groups_with_azure = recurse_attrs(keycloak_admin, keycloak_admin.get_groups())
    print(groups_with_azure)
    update_user(keycloak_admin, user, groups_with_azure)
