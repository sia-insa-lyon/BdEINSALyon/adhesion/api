from django.contrib import admin

from laverie import models


@admin.register(models.Laverie)
class BasicAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'tokens', 'has_valid_membership')
    pass
