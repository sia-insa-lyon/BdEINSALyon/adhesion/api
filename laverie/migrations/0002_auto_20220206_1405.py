# Generated by Django 3.1.13 on 2022-02-06 13:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0027_auto_20201113_2122'),
        ('laverie', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='laverie',
            name='member_VA',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='api.member', unique=True, verbose_name='membre VA'),
        ),
        migrations.AlterField(
            model_name='laverie',
            name='rfid',
            field=models.CharField(max_length=255, unique=True, verbose_name='RFID'),
        ),
        migrations.AlterField(
            model_name='laverie',
            name='student_number',
            field=models.CharField(max_length=255, unique=True, verbose_name='numéro étudiant'),
        ),
    ]
