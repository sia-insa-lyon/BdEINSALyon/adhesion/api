from adhesion.api.models import Member
from django.db import models
from django.utils.translation import gettext_lazy as _


class Laverie(models.Model):
    first_name = models.CharField(max_length=255, verbose_name=_('prénom'))
    last_name = models.CharField(max_length=255, verbose_name=_('nom'))
    tokens = models.PositiveIntegerField(verbose_name=_('jetons'))
    student_number = models.CharField(max_length=255, verbose_name=_('numéro étudiant'), unique=True)
    rfid = models.CharField(max_length=255, verbose_name=_('RFID'), unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    member_VA = models.OneToOneField(Member, null=True, blank=True, verbose_name=_('membre VA'),
                                     on_delete=models.CASCADE)

    @property
    def has_valid_membership(self):
        if(self.member_VA):
            return self.member_VA.has_valid_membership
        else:
            return False

    @property
    def cards_va(self):
        if (self.member_VA):
            return self.member_VA.cards
        else:
            return False

    def __str__(self):
        return "{} {} Nombre jetons: {}".format(self.first_name, self.last_name, self.tokens)
