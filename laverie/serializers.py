from rest_framework import serializers

from adhesion.api.serializers import CardSerializer
from laverie.models import Laverie
from adhesion.api.serializers.standard import MemberSerializer
from adhesion.api import models

class LaverieSerializer(serializers.ModelSerializer):
    member_VA = MemberSerializer(allow_null=True, required=False)

    class Meta:
        model = Laverie
        fields = '__all__'


class LaverieSerializerOutput(serializers.ModelSerializer):
    class Meta:
        model = Laverie
        fields = (
            'id',
            'first_name',
            'last_name',
            'has_valid_membership',
            'has_valid_card',
            'student_number',
            'tokens',
            'rfid',
            'created_at',
            'updated_at'
        )

class MemberLaverieGetterSerializer(serializers.ModelSerializer):
    card = CardSerializer(allow_null=True, read_only=True)
    class Meta:
        model = models.Member
        fields = (
            'first_name', 'last_name','card'
        )
