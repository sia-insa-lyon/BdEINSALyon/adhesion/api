from django.urls import path
from rest_framework.routers import DefaultRouter

from laverie import views

router = DefaultRouter()
router.register('laverie', views.LaverieViewSet, 'laverie')
urlpatterns = [
                    path('get_informations', views.get_information)
              ] + router.urls
