
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from adhesion.api.permissions import *
from laverie.models import Laverie
from laverie.serializers import LaverieSerializer, LaverieSerializerOutput, MemberLaverieGetterSerializer

# Model View Set utilisé exclusivment par le client laverie 2.0
class LaverieViewSet(viewsets.ModelViewSet):
    queryset = Laverie.objects.all()
    serializer_class = LaverieSerializerOutput
    permission_classes = [HasRoleLaverie]
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    filter_fields = ('tokens','student_number','rfid')
    """
    Trois champs de query possible pour récupérer l'id du compte:
    numéro étudiant
    numéro de carte VA
    numéro rfid
    """
    search_fields = ('=student_number', "=rfid")

    def create(self, request, *args, **kwargs):
        """
        Il y a 2 inscriptions possible:
        1 - Avec VA donc liens avec le compte adhésion
        2 - Sans VA
        """
        neededData = ['first_name','last_name','student_number','rfid','tokens']
        if not any(x in request.data for x in neededData):
            return Response("Merci de bien fournir les champs minimum requis, 'first_name','last_name','student_number','rfid','tokens'",
                            status=status.HTTP_400_BAD_REQUEST)
        laverie = Laverie(
            first_name=request.data['first_name'],
            last_name=request.data['last_name'],
            student_number=request.data['student_number'],
            rfid=request.data['rfid'],
            tokens=request.data['tokens']
        )
        # Si la personne possède un compte VA valide
        if 'num_va' in request.data:
            try:
                membre = Member.objects.get(cards__code=request.data['num_va'])
                if membre.has_valid_membership:
                    laverie.member_VA = membre
                else:
                    return Response("La carte VA n'est plus valide",
                                    status=status.HTTP_400_BAD_REQUEST)
            except Member.DoesNotExist:
                raise NotFound()
        serializer = LaverieSerializer(data=laverie.__dict__)
        if serializer.is_valid():
            laverie.save()
            return Response(True, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# Endpoint pour récuperer un adhérent sur la DB Membre
@api_view(['POST'])
@permission_classes([HasRoleLaverie])
def get_information(request):
    if 'student_number' not in request.data:
        return Response("Impossible de trouver le numéro étudiant dans la requête ('student_number' missing) ",
                        status=status.HTTP_400_BAD_REQUEST)
    try:
        membre = Member.objects.get(student_profile__student_number=request.data['student_number'])
        if membre.has_valid_membership:
            output = MemberLaverieGetterSerializer(membre).data
            code_statut = status.HTTP_200_OK
        else:
            output = "Le carte VA n'est pas relié à un compte en cours de validité"
            code_statut = status.HTTP_200_OK
        return Response(output, code_statut)
    except Member.DoesNotExist:
        return Response("Les informations ne permettent pas de trouver un adhérent", status=status.HTTP_404_NOT_FOUND)

