from django.urls import path
from rest_framework.routers import DefaultRouter

from mercanet import views

router = DefaultRouter()
router.register('payment', views.MercanetPaymentViewset, 'payment')
router.register('admin', views.MercanetAdminViewset, 'admin')
urlpatterns = [
                  # path('payment/', views.MercanetPaymentViewset.as_view()),
                  path("auto/", views.mercanet_response, name="auto"),
                  path("response_client/", views.mercanet_reponse_client, name='response_client')
              ] + router.urls
