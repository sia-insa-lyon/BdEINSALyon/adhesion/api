# Create your views here.
import decimal
import json
import logging

import requests
from django.conf import settings
from django.db import transaction
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import APIException
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet, ReadOnlyModelViewSet

from adhesion.api.models import Membership, Member
from adhesion.api.permissions import HasMember, HasRoleTrezo
from customer.payment import update_customer_data_on_mercanet_payment_success
from mercanet.models import TransactionMercanet, STATUS_PAYING, STATUS_REQUEST, STATUS_CREATED
from mercanet.serializers import TransactionMercanetSerializer, seal_transaction, PaymentRequestSerializer, \
    TransactionMercanetAdminSerializer
from mercanet.utils import genId, fromVomi, seal_hmac_sha256_from_string, responseCodes

logger = logging.getLogger('mercanet')


class MercanetPaymentViewset(ViewSet):
    """
    Paiement en deux étapes:
     * création de la transaction sur notre serveur & calcul du prix
     * envoi de la transaction à MercaNET pour obtenir les infos de redirection client
    """
    permission_classes = [IsAuthenticated, HasMember]

    @transaction.atomic
    def create(self, request):
        payment_request = PaymentRequestSerializer(request.data).data
        print(payment_request)
        membership: Membership = Membership.objects.get(pk=int(payment_request['membership']))
        # bizuth: Bizuth = None
        # if payment_request.get('bizuth', None) is not None and not settings.WEI_PAYMENT_DISABLED:
        #    bizuth = Bizuth.objects.get(pk=int(payment_request['bizuth']))
        member: Member = self.request.user.member

        if member.has_valid_membership:
            raise APIException('vous avez déjà payé !')

        amount = membership.membership.price_ttc
        # if bizuth is not None:
        #    if bizuth.a_paye:
        #        raise APIException(code=status.HTTP_400_BAD_REQUEST, detail="Vous avez déjà un objet Bizuth payé !")
        #    amount += settings.WEI_PRICE_TTC
        amount = amount * 100  # conversion en Euros

        queryset = member.transactions_mercanet.filter(
            membership=membership,
            responseCode=None,
            status__in=[STATUS_REQUEST, STATUS_CREATED],
            # on ne peut pas réutiliser une transaction qui est en cours de
            # paiement
        )
        if queryset.count() > 1:
            mrd = queryset.first()
            logger.debug("previous unfinished transaction found, reusing it")
            return Response(status=status.HTTP_200_OK, data=TransactionMercanetSerializer(instance=mrd).data)

        else:
            logger.debug("no previous transactino found, creating one")
            mrd = TransactionMercanet(
                issuer=self.request.user.member,
                amount=decimal.Decimal(amount).to_integral_value(decimal.ROUND_UP),
                normalReturnUrl=settings.MERCANET['RETURN_URL'],
                returnContext=json.dumps({'member': member.pk,
                                          'membership': membership.pk,
                                          }),  # 'bizuth': bizuth.pk if bizuth is not None else None}),
                membership=membership,
            )
            mrd.save()  # pour recup l'ID
            mrd.transactionReference = genId(member.id, membership.id, mrd.id)
            mrd.save()
            return Response(status=status.HTTP_201_CREATED, data=TransactionMercanetSerializer(instance=mrd).data)

    @action(detail=True, methods=['get'])
    def pay(self, request, pk):
        """
        Démarre le paiment chez mercanet: notre serveur demande à Mercanet la page sur laquelle nous
        devrons rediriger l'utilisateur pour qu'il rentre sa CB
        """
        mrd: TransactionMercanet = TransactionMercanet.objects.get(id=pk)
        request_data = seal_transaction(mrd)
        print(request_data)

        mercanet_response = requests.post(settings.MERCANET["URL"], json=request_data)
        mercanet_response_data = mercanet_response.json()

        mrd_after = TransactionMercanet.objects.get(id=pk)
        mrd_after.status = STATUS_REQUEST
        mrd_after.save()

        if mercanet_response_data['redirectionStatusCode'] == '00':
            mrd_after.status = STATUS_PAYING
            mrd_after.save()
            return Response(status=status.HTTP_200_OK, data={
                "redirectionUrl": mercanet_response_data["redirectionUrl"],
                "interfaceVersion": mrd.interfaceVersion,
                "redirectionData": mercanet_response_data["redirectionData"],
            })

        else:
            raise APIException(code=status.HTTP_424_FAILED_DEPENDENCY,
                               detail=f"Erreur lors de la communication avec la banque : {str(mercanet_response)} ({mercanet_response_data})")

    def list(self, request):
        queryset = TransactionMercanet.objects.filter(orderId__in=self.request.user.member.memberships)
        return Response(TransactionMercanetSerializer(queryset, many=True).data)


@csrf_exempt
def mercanet_response(request):
    """
    Appelée par MercaNET lui-même lors de la réponse automatique
    """
    if request.method != 'POST':
        return HttpResponse("Bad Request", status=status.HTTP_400_BAD_REQUEST)
    data = request.POST["Data"]
    propre = fromVomi(data)
    seal = request.POST["Seal"]

    logger.info(f"Receiving autoMercanet with seal {seal} : {propre} (from {data})")

    recalc_seal = seal_hmac_sha256_from_string(
        data, secret=settings.MERCANET["SECRET_KEY"]
    )  # on met non pas une concaténation des champs mais les données brutes (et moches) de mercanet
    if seal != recalc_seal:
        logger.warning("Seal error ! Bank servers may be under attack or we have a bad key")
        return HttpResponse()
    serializer = TransactionMercanetSerializer(data=propre)

    if serializer.is_valid():
        transaction_reference = serializer.validated_data['transactionReference']
        transaction = TransactionMercanet.objects.get(transactionReference=transaction_reference)
        if not settings.DEBUG and transaction.terminated:  # on n'enregistre pas une 2e fois la transaction
            return HttpResponse("vous avez répondu une 2e fois !")  # c'est arrivé une fois que mercanet réponde 2 fois
        serializer.update(transaction, serializer.validated_data)
        update_customer_data_on_mercanet_payment_success(transaction)

    else:
        logger.warning(f" erreurs de déserialisation réponse MecaNET: {serializer.errors} ({propre})")

    print(f"id:{propre['orderId']} {propre['returnContext']}")
    return HttpResponse('merci et a bientot')


@csrf_exempt
def mercanet_reponse_client(request):
    """
    Appelée par le navigateur du client lors de la redirection
    ATTENTION, c'est cet endpoint qui est le plus susceptible d'être piraté
    (même s'il faudrait casser la clé secrète MercaNET)
    """
    data = request.POST["Data"]
    propre = fromVomi(data)
    seal = request.POST["Seal"]

    recalc_seal = seal_hmac_sha256_from_string(
        data, secret=settings.MERCANET["SECRET_KEY"]
    )  # on met non pas une concaténation des champs mais les données brutes (et moches) de mercanet
    if seal != recalc_seal:
        logger.warning("Seal error ! Bank servers may be under attack or we have a bad key")
        return HttpResponse('petit malin !')
    else:
        logger.info(f"Receiving valid clientMercanet response : {propre} (from {data})")
        serializer = TransactionMercanetSerializer(data=propre)
        if serializer.is_valid():
            transaction_reference = serializer.validated_data['transactionReference']
            transaction: TransactionMercanet = TransactionMercanet.objects.get(
                transactionReference=transaction_reference)
            if transaction.payed:
                return HttpResponseRedirect(f"{settings.PUBLIC_FRONTEND_URL}/return")
            else:
                return render(request, 'mercanet_erreur.html', {
                    'transaction': transaction,
                    'detail': responseCodes.get(transaction.responseCode, "code réponse inconnu"),
                    'redirect': f"{settings.PUBLIC_FRONTEND_URL}/return?code={transaction.responseCode or 'null'}",
                })


class MercanetAdminViewset(ReadOnlyModelViewSet):
    queryset = TransactionMercanet.objects.all()
    serializer_class = TransactionMercanetAdminSerializer
    permission_classes = [HasRoleTrezo]
    filter_backends = (SearchFilter, DjangoFilterBackend)
    filter_fields = ('membership', 'responseCode', 'status', 'created_at', 'updated_at', 'amount')
    search_fields = ('$issuer__first_name', '$issuer__last_name', '^issuer__email',
                     '=issuer__student_profile__student_number', '=issuer__cards__code',
                     '=transactionReference')

    def list(self, request, *args, **kwargs):
        start = request.query_params.get('start_datetime', None)
        stop = request.query_params.get('stop_datetime', None)
        queryset = self.filter_queryset(self.get_queryset())

        if start is not None and start is not None:
            queryset = queryset.filter(transactionDateTime__gte=start,
                                       transactionDateTime__lte=stop)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
