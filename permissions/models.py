import datetime

from django.contrib.auth import models as auth_models
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from adhesion.api.models import Member, Card


class User(AbstractUser):
    """
    Represents a user in our app.
    I implemented this class because I needed a custom behavior for is_staff and is_superuser.
    This class also fetches Azure groups whenever it's needed so the user's permissions
    are kept up to date.
    """

    _is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    _is_superuser = models.BooleanField(
        _('superuser status'),
        default=False,
        help_text=_(
            'Designates that this user has all permissions without '
            'explicitly assigning them.'
        ),
    )

    def __init__(self, *args, **kwargs):
        self._is_staff = kwargs.get('is_staff', False)
        self._is_superuser = kwargs.get('is_superuser', False)
        super().__init__(*args, **kwargs)

        if self.id:
            try:
                if self.member:
                    if self.member.has_valid_membership and self.user_level < 1:
                        self.user_level = 1
            except:
                pass
            self.save()

    @property
    def is_staff(self):
        return self._is_staff or self.groups.filter(name__icontains='admin').exists()

    @is_staff.setter
    def is_staff(self, is_staff):
        self._is_staff = is_staff

    @property
    def is_superuser(self):
        return self._is_superuser or self.groups.filter(name__icontains='admin').exists()

    @is_superuser.setter
    def is_superuser(self, is_superuser):
        self._is_superuser = is_superuser

    @property
    def member(self):
        try:
            return Member.objects.get(email=self.email)
        except Member.DoesNotExist:
            return None

    @property
    def cards(self):
        try:
            return self.member.cards
        except Member.DoesNotExist:
            return None
        except Card.DoesNotExist:
            return None
