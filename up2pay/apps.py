from django.apps import AppConfig


class UpToPayConfig(AppConfig):
    name = "up2pay"
