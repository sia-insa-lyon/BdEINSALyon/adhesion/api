import html
import urllib.parse
from datetime import datetime, timezone, timedelta

from django.conf import settings
from django.core.validators import RegexValidator
from django.db import models

from adhesion.api.models import Member, Membership

secret = settings.MERCANET["SECRET_KEY"]
alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$',
                              'Only alphanumeric characters are allowed.')  # merci @martijns-peter https://stackoverflow.com/a/17165415/12643213

STATUS_CREATED = 'CREATED'  # transaction créée par le client
STATUS_PAYING = 'PAYING'  # infos de paiement envoyées au client
STATUS_PAYED = 'PAYED'  # réponse IPN de Up2Pay reçue et valide
STATUS_PAYMENT_FAILED = 'PAYMENT_FAILED'  # Échec du paiement
STATUS_FAILED = 'FAILED'  # échec à une autre étape


class TransactionUp2Pay(models.Model):
    """
    Représente une transaction 'immutable'
    Comme les références de transaction sont uniques, une transaction ne peut être 'rejouée', il faut donc
    en créer une nouvelle si celle-ci échoue
    """

    class Meta:
        verbose_name = "Transaction Up2Pay"
        verbose_name_plural = "Transactions Up2Pay"

    transaction_reference = models.CharField(
        unique=True,
        max_length=35,
        validators=[alphanumeric], null=True)  # format : AN35 -> alphanumérique

    amount = models.IntegerField(verbose_name="Montant (centimes)")
    membership = models.ForeignKey(to=Membership, verbose_name="adhésion à payer",
                                   related_name='transactions_m_up2pay',
                                   on_delete=models.CASCADE)

    normal_return_url = models.URLField(verbose_name="URL de retour client", max_length=1023)  # on peut le faire varier

    # Les champs suivants sont remplis par la réponse auto Up2Pay
    transaction_datetime = models.DateTimeField(null=True, verbose_name="Date du paiement")
    response_code = models.CharField(null=True, verbose_name="Code de réponse", max_length=5)

    # Champs cachés à Up2Pay
    issuer = models.ForeignKey(to=Member, null=False, verbose_name="Adhérent", related_name='transactions_up2pay',
                               on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Date de création")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Date de mise à jour")

    status = models.CharField(max_length=63, verbose_name="État", choices=(
        (STATUS_CREATED, 'CREATED'),
        (STATUS_PAYING, 'PAYING'),
        (STATUS_PAYED, 'PAYED'),
        (STATUS_PAYMENT_FAILED, 'PAYMENT_FAILED'),
        (STATUS_FAILED, 'FAILED'),
    ), default=STATUS_CREATED)

    def to_up2pay_form_data(self):
        phone_without_country_code = str(self.issuer.phone)[-9:]
        phone_country_code = str(self.issuer.phone)[:-9]
        billing = ("<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                   "<Billing>"
                   "<Address>"
                   f"<FirstName>{self.issuer.first_name}</FirstName>"
                   f"<LastName>{self.issuer.last_name}</LastName>"
                   "<Address1>1 example street</Address1>"
                   "<ZipCode>01010</ZipCode>"
                   "<City>Example</City>"
                   "<CountryCode>250</CountryCode>"
                   f"<MobilePhone>0{phone_without_country_code}</MobilePhone>"
                   f"<CountryCodeMobilePhone>{phone_country_code}</CountryCodeMobilePhone>"
                   "</Address>"
                   "</Billing>")

        shopping_cart = ("<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                         "<shoppingcart>"
                         "<total><totalQuantity>1</totalQuantity>"
                         "</total>"
                         "</shoppingcart>")

        current_dt = datetime.now(timezone(timedelta(hours=1)))  # Heure française
        formatted_dt = current_dt.strftime('%Y-%m-%dT%H:%M:%S%z')
        formatted_dt = formatted_dt[:22] + ':' + formatted_dt[22:]

        return {
            "PBX_SITE": settings.UP2PAY["PBX_SITE"],
            "PBX_RANG": settings.UP2PAY["PBX_RANG"],
            "PBX_IDENTIFIANT": settings.UP2PAY["PBX_IDENTIFIANT"],
            "PBX_TOTAL": self.amount,
            "PBX_DEVISE": "978",
            "PBX_CMD": f"{self.id}",
            "PBX_SOURCE": "RWD",
            "PBX_PORTEUR": self.issuer.email,
            "PBX_RETOUR": "id:R;response_code:E;sign=K",
            "PBX_HASH": "SHA512",
            "PBX_TIME": formatted_dt,
            "PBX_SHOPPINGCART": shopping_cart,
            "PBX_BILLING": billing,
            "PBX_SOUHAITAUTHENT": "01",
            "PBX_REPONDRE_A": settings.UP2PAY["PBX_REPONDRE_A"],
            "PBX_EFFECTUE": self.normal_return_url,
            "PBX_REFUSE": self.normal_return_url,
            "PBX_ANNULE": f"{self.normal_return_url}cancel/",
            "PBX_ATTENTE": self.normal_return_url
        }

    def __str__(self):
        return f"Transaction #{self.id or 0} adhésion #{self.membership.id} de {self.amount / 100.0} €"

    @property
    def payed(self):
        return self.response_code == "00000"

    @property
    def terminated(self):
        return self.response_code is not None and self.response_code != ""
