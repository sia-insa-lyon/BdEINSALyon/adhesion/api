from rest_framework import serializers

from adhesion.api.serializers.standard import MembershipSerializer, MemberSerializer
from up2pay.models import TransactionUp2Pay


class TransactionUp2PaySerializer(serializers.ModelSerializer):
    class Meta:
        exclude = ['normal_return_url', 'issuer', 'created_at', 'updated_at', 'status', 'membership']
        model = TransactionUp2Pay

    transaction_reference = serializers.CharField(required=True)  # il faut le redéfinir pour enlever le 'unique'



class PaymentRequestSerializer(serializers.Serializer):
    membership = serializers.IntegerField(required=True)
    bizuth = serializers.IntegerField(required=False)


class TransactionUp2PayAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionUp2Pay
        exclude = []

    issuer = MemberSerializer()
    membership = MembershipSerializer()
