from django.urls import path
from rest_framework.routers import DefaultRouter

from up2pay import views

router = DefaultRouter()
router.register('payment', views.Up2PayPaymentViewset, 'payment')
router.register('admin', views.Up2PayPaymentViewset, 'admin')

urlpatterns = [
                  path("auto/", views.up2pay_response, name="auto"),
                  path("response_client/", views.up2pay_client_response, name='response_client'),
                  path("response_client/cancel/", views.up2pay_client_response_cancel, name="response_client_cancel")
              ] + router.urls
