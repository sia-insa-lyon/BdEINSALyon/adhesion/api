# Create your views here.
import decimal
import logging
from datetime import datetime

from django.conf import settings
from django.db import transaction
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import APIException
from rest_framework.filters import SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet, ReadOnlyModelViewSet

from adhesion.api.models import Membership, Member
from adhesion.api.permissions import HasMember, HasRoleTrezo
from customer.payment import update_customer_data_on_up2pay_payment_success
from up2pay.models import TransactionUp2Pay, STATUS_CREATED, STATUS_PAYING, STATUS_FAILED
from up2pay.serializers import PaymentRequestSerializer, TransactionUp2PaySerializer, TransactionUp2PayAdminSerializer
from up2pay.utils import genId, seal_hmac_sha512_from_string, get_client_ip, check_seal_sha1_from_dict_and_up2pay_public_key, \
    get_response_code_detail

logger = logging.getLogger('up2pay')


class Up2PayPaymentViewset(ViewSet):
    """
    Paiement en deux étapes:
     * création de la transaction sur notre serveur & calcul du prix
     * préparation des données de paiement pour le client qui soumettra un formulaire POST à Up2Pay.
    """
    permission_classes = [IsAuthenticated, HasMember]

    @transaction.atomic
    def create(self, request):
        payment_request = PaymentRequestSerializer(request.data).data
        print(payment_request)
        membership: Membership = Membership.objects.get(pk=int(payment_request['membership']))

        member: Member = self.request.user.member

        valid_same_memberships = Membership.objects.filter(member=member, membership=membership.membership, valid=True)
        if valid_same_memberships.count() > 0:
            raise APIException('vous avez déjà payé !')

        amount = membership.membership.price_ttc * 100  # conversion en centimes d'Euros

        # Récupération d'une éventuelle transaction en cours, mais qui n'est pas en cours de paiement.
        queryset = member.transactions_up2pay.filter(
            membership=membership,
            response_code=None,
            status=STATUS_CREATED,
        )
        if queryset.count() > 1:
            upd = queryset.first()
            logger.debug("previous unfinished transaction found, reusing it")
            return Response(status=status.HTTP_200_OK, data=TransactionUp2PaySerializer(instance=upd).data)

        else:
            logger.debug("no previous transaction found, creating one")
            upd = TransactionUp2Pay(
                issuer=self.request.user.member,
                amount=decimal.Decimal(amount).to_integral_value(decimal.ROUND_UP),
                normal_return_url=settings.UP2PAY['RETURN_URL'],
                membership=membership,
            )
            upd.save()  # pour recup l'ID
            upd.transaction_reference = genId(member.id, membership.id, upd.id)
            upd.save()
            return Response(status=status.HTTP_201_CREATED, data=TransactionUp2PaySerializer(instance=upd).data)

    @action(detail=True, methods=['get'])
    def pay(self, request, pk):
        """
        Renvoie au client les informations nécessaires pour effectuer le paiement
        """

        upd: TransactionUp2Pay = TransactionUp2Pay.objects.get(pk=pk)
        upd.status = STATUS_PAYING
        upd.save()

        data = upd.to_up2pay_form_data()

        data_str = "&".join([f"{k}={v}" for k, v in data.items()])
        data["PBX_HMAC"] = seal_hmac_sha512_from_string(data_str, settings.UP2PAY['SECRET_KEY'])

        return Response(status=status.HTTP_200_OK, data={
            "form": data,
            "url": settings.UP2PAY['UP2PAY_URL'],
        })

    def list(self, request):
        queryset = TransactionUp2Pay.objects.filter(orderId__in=self.request.user.member.memberships)
        return Response(TransactionUp2PaySerializer(queryset, many=True).data)


@csrf_exempt
def up2pay_response(request):
    """
    Appelée par Up2Pay lui-même lors de la réponse automatique (IPN)
    Variables IPN : PBX_RETOUR = id:R;response_code:E;sign=K
    """

    id = request.GET.get('id')
    response_code = request.GET.get('response_code')

    logger.info(f"Up2Pay IPN: id={id}, response_code={response_code}")

    # check the source IP
    if get_client_ip(request) not in settings.UP2PAY['IPN_IPS']:
        logger.warning(f"IPN from unauthorized IP: {get_client_ip(request)}")
        return HttpResponse(status=403)

    # check the seal
    seal_dict = {"id": id, "response_code": response_code}
    if not check_seal_sha1_from_dict_and_up2pay_public_key(seal_dict, request.GET.get('sign')):
        logger.warning(f"IPN with invalid seal: {request.GET.get('sign')} (content: {seal_dict})")
        logger.debug(f"All GET parameters: {request.GET}")
        return HttpResponse(status=403)

    # update the transaction
    upd = TransactionUp2Pay.objects.get(pk=id)
    upd.datetime = datetime.now()
    upd.response_code = response_code
    upd.save()
    update_customer_data_on_up2pay_payment_success(upd)

    return HttpResponse(status=200)


@csrf_exempt
def up2pay_client_response_cancel(request):
    """
    Appelée par le navigateur du client lors de l'annulation du paiement
    """
    return HttpResponseRedirect(f"{settings.PUBLIC_FRONTEND_URL}/authenticated/paymentreturn")


@csrf_exempt
def up2pay_client_response(request):
    """
    Appelée par le navigateur du client lors de la redirection
    Variables : PBX_RETOUR = id:R;response_code:E;sign=K
    """
    id = request.GET.get('id')
    response_code = request.GET.get('response_code')

    logger.info(f"Up2Pay client response: id={id}, response_code={response_code}")

    upd = TransactionUp2Pay.objects.get(pk=id)

    # check the seal
    seal_dict = {"id": id, "response_code": response_code}
    if not check_seal_sha1_from_dict_and_up2pay_public_key(seal_dict, request.GET.get('sign')):
        logger.warning(f"Client return with invalid seal: {request.GET.get('sign')} (content: {seal_dict})")
        logger.debug(f"All GET parameters: {request.GET}")

        return render(request, 'up2pay_erreur.html', {
            'transaction': upd,
            'detail': "AD1",
            'redirect': f"{settings.PUBLIC_FRONTEND_URL}/authenticated/paymentreturn?code=AD1",
        })

    # Get the transaction status
    if not upd.terminated:
        logger.warning(f"Client return before IPN received. An issue may have occurred.")
        code = "AD2" if response_code == "00000" else "AD3"
        return render(request, 'up2pay_erreur.html', {
            'transaction': upd,
            'detail': get_response_code_detail(code),
            'redirect': f"{settings.PUBLIC_FRONTEND_URL}/authenticated/paymentreturn?code={code}",
        })

    logger.info(f"Up2Pay client response has a matching terminated transaction: "
                f"id={id}, response_code={upd.response_code}")
    if upd.payed:
        return HttpResponseRedirect(f"{settings.PUBLIC_FRONTEND_URL}/authenticated/paymentreturn?code={upd.response_code}")

    return render(request, 'up2pay_erreur.html', {
        'transaction': upd,
        'detail': get_response_code_detail(upd.response_code),
        'redirect': f"{settings.PUBLIC_FRONTEND_URL}/authenticated/paymentreturn?code={upd.response_code or 'null'}",
    })


class Up2PayAdminViewset(ReadOnlyModelViewSet):
    queryset = TransactionUp2Pay.objects.all()
    serializer_class = TransactionUp2PayAdminSerializer
    permission_classes = [HasRoleTrezo]
    filter_backends = (SearchFilter, DjangoFilterBackend)
    filter_fields = ('membership', 'response_code', 'status', 'created_at', 'updated_at', 'amount')
    search_fields = ('$issuer__first_name', '$issuer__last_name', '^issuer__email',
                     '=issuer__student_profile__student_number', '=issuer__cards__code',
                     '=transaction_reference')

    def list(self, request, *args, **kwargs):
        pass
        start = request.query_params.get('start_datetime', None)
        stop = request.query_params.get('stop_datetime', None)
        queryset = self.filter_queryset(self.get_queryset())

        if start is not None and start is not None:
            queryset = queryset.filter(transactionDateTime__gte=start,
                                       transactionDateTime__lte=stop)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
