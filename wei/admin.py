from django.contrib import admin

# Register your models here.
from wei import models


@admin.register(models.Bizuth)
@admin.register(models.Bus)
@admin.register(models.Bungalow)
class DummyAdmin(admin.ModelAdmin):
    pass
