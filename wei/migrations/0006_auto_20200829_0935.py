# Generated by Django 3.1 on 2020-08-29 07:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wei', '0005_auto_20200814_1543'),
    ]

    operations = [
        migrations.AddField(
            model_name='bizuth',
            name='paiement_recu',
            field=models.BooleanField(default=False, verbose_name='Paiement du WEI reçu'),
        ),
        migrations.AlterField(
            model_name='bizuth',
            name='a_paye',
            field=models.BooleanField(default=False, verbose_name='WEI payé et encaissé'),
        ),
        migrations.AlterField(
            model_name='bizuth',
            name='caution_valide',
            field=models.BooleanField(default=False, verbose_name='Chèque de caution reçu au BdE'),
        ),
        migrations.AlterField(
            model_name='bizuth',
            name='participant',
            field=models.BooleanField(default=False, verbose_name='Inscrit sur la liste des possibles participants'),
        ),
    ]
