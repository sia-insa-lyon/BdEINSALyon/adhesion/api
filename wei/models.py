import datetime
from math import modf

from dateutil.relativedelta import relativedelta
from django.db import models
# Create your models here.
from django.utils.dateparse import parse_date
from django.utils.timezone import now

from adhesion import settings
from adhesion.api.models import Member


def decharge_parentale_storagename(instance, user_provided_filename):
    # type: (Bizuth, str)->str
    return f"decharges_parentales/{instance.id}-{instance.member.email}-{user_provided_filename}"


class Bus(models.Model):
    nom = models.CharField(max_length=255, verbose_name="Nom du Bus")
    places = models.IntegerField(verbose_name="Nombre total de places")

    def __str__(self):
        return self.nom

    @property
    def places_occupees(self):
        return self.bizuths.filter(participant=True).count()

    @property
    def places_restantes(self):
        return self.places - self.places_occupees


GENRES = [
    ('M', "Homme"),
    ('W', "Femme"),
    ('I', 'Indéfini/Inconnu')
]


class Bungalow(models.Model):
    nom = models.CharField(verbose_name="Nom du bungalow", max_length=255)
    genre = models.CharField(max_length=1, choices=GENRES)
    places = models.IntegerField(verbose_name="Nombre de places")
    bus = models.ForeignKey(to=Bus, null=True, on_delete=models.PROTECT)

    def __str__(self):
        return self.nom

    @property
    def places_occupees(self):
        return self.bizuths.filter(participant=True).count()

    @property
    def places_restantes(self):
        return self.places - self.places_occupees


class Bizuth(models.Model):
    # partie modifiable par le bizuth lui-même
    member = models.OneToOneField(to=Member, related_name='bizuth', verbose_name="Membre adhérent", null=False,
                                  on_delete=models.CASCADE)
    regime_particulier = models.TextField(verbose_name="Régime particulier", null=True, blank=True)
    informations_speciales = models.TextField(verbose_name='Informations particulières', null=True, blank=True)
    decharge_parentale = models.FileField(verbose_name="Décharge parentale signée (si mineur)",
                                          upload_to=decharge_parentale_storagename, blank=True)

    # partie modifiable uniquement par les orgas
    caution_valide = models.BooleanField(verbose_name='Chèque de caution reçu au BdE', default=False)
    autorisation_parentale_valide = models.BooleanField(verbose_name="Autorisation parentale vérifiée", default=False)
    refunded = models.BooleanField(verbose_name="Inscription remboursée (si annulation)", default=False)
    bus = models.ForeignKey(verbose_name='Bus', to=Bus, related_name='bizuths', on_delete=models.CASCADE, null=True)
    bungalow = models.ForeignKey(verbose_name='Bungalow', to=Bungalow, related_name='bizuths', on_delete=models.CASCADE,
                                 null=True)
    participant = models.BooleanField(verbose_name="Inscrit sur la liste des possibles participants",
                                      default=False)  # dépend aussi de la liste d'attente

    prioritaire_file = models.BooleanField(verbose_name="N'a jamais été participant par le passé",
                                      default=True)  # Permet de savoir si un particpant est prioritaire dans la file
                                                     # d'attente
    # lecture seule
    date_inscription = models.DateTimeField(verbose_name="Date d'inscription", default=now, blank=True)
    paiement_recu = models.BooleanField(verbose_name="Paiement du WEI reçu",
                                        default=False)  # Quand le paiement est seulement reçu ; ne valide pas l'inscription

    payment_method = models.IntegerField(verbose_name='Moyen de paiement', default=3, choices=(
        (1, 'CB'),
        (2, 'Espèce'),
        (3, 'Chèque'),
        (4, 'En ligne')
    ))

    def __str__(self):
        return f"Bizuth {self.member}"

    @property
    def age_au_wei(self):
        u, ans = modf((parse_date(settings.DATE_WEI) - self.member.birthday).days / 365.2425)
        return f"{int(ans)} ans et {int(u / 30 * 365)} mois"

    @property
    def est_mineur(self):
        date_wei = parse_date(settings.DATE_WEI)
        anniv_minimal = datetime.date(year=date_wei.year - 18,
                                      month=date_wei.month,
                                      day=date_wei.day)
        return self.member.birthday > anniv_minimal
