from rest_framework import serializers

from wei.models import Bizuth


class PublicBizuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bizuth
        fields = ('regime_particulier', 'informations_speciales', 'decharge_parentale', 'date_inscription', 'id')

    date_inscription = serializers.DateTimeField(read_only=True)
