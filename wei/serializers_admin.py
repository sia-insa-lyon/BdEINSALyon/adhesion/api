from rest_framework import serializers

from adhesion.api.serializers import MemberSerializer
from wei.models import *


class BusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bus
        exclude = []

    places_occupees = serializers.IntegerField(read_only=True)
    places_restantes = serializers.IntegerField(read_only=True)


class BungalowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bungalow
        exclude = []

    places_occupees = serializers.IntegerField(read_only=True)
    places_restantes = serializers.IntegerField(read_only=True)


class TableauBizuthsMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ['id', 'first_name', 'last_name', 'has_valid_membership', 'has_valid_card', 'va_cheque_received']


class TableauBizuthsBusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bus
        fields = ['nom']


class TableauBizuthsBungalowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bungalow
        fields = ['nom']


class TableauBizuthsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bizuth
        fields = ['id', 'member', 'date_inscription', 'paiement_recu', 'caution_valide', 'est_mineur',
                  'autorisation_parentale_valide', 'participant', 'prioritaire_file', 'bus', 'bungalow']

    member = TableauBizuthsMemberSerializer(read_only=True)
    est_mineur = serializers.BooleanField(read_only=True)
    bungalow = TableauBizuthsBungalowSerializer(read_only=True)
    bus = TableauBizuthsBusSerializer(read_only=True)


class AdminBizuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bizuth
        exclude = []

    member = MemberSerializer(read_only=True)
    est_mineur = serializers.BooleanField(read_only=True)
    age_au_wei = serializers.CharField(read_only=True)
    decharge_parentale = serializers.FileField(read_only=True)


class AdminBizuthReadSerializer(AdminBizuthSerializer):
    class Meta:
        model = Bizuth
        exclude = []

    bus = BusSerializer(read_only=True)
    bungalow = BungalowSerializer(read_only=True)


class AdminBizuthWriteSerializer(AdminBizuthSerializer):
    decharge_parentale = serializers.FileField(read_only=True)


class AdminBizuthCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bizuth
        exclude = []


class AdminBusBungalowList(serializers.ModelSerializer):
    class Meta:
        model = Bizuth
        fields = ['member', 'bus', 'bungalow', 'est_mineur']
        depth = 1


class ExportBizuthsMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ['first_name', 'last_name', 'gender', 'phone', 'email']


class ExportBizuthsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bizuth
        fields = ['member', 'age_au_wei', 'est_mineur', 'caution_valide',
                  'autorisation_parentale_valide', 'bus', 'bungalow']

    member = ExportBizuthsMemberSerializer(read_only=True)
    age_au_wei = serializers.CharField(read_only=True)
    est_mineur = serializers.BooleanField(read_only=True)
    bus = BusSerializer(read_only=True)
    bungalow = BungalowSerializer(read_only=True)


class ExportRegimesMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ['first_name', 'last_name']


class ExportRegimesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bizuth
        fields = ['member', 'informations_speciales', 'regime_particulier']

    member = ExportRegimesMemberSerializer(read_only=True)
