from django.urls import path, include
from rest_framework.routers import DefaultRouter

from wei import views, views_admin

adminrouter = DefaultRouter()
adminrouter.register('bizuths', views_admin.AdminBizuthViewset, 'bizuths')
adminrouter.register('bus', views_admin.BusViewset, 'bus')
adminrouter.register('bungalow', views_admin.BungalowViewset, 'bungalow')

admin_urls = [
                 path('stats/', views_admin.stats_view)
             ] + adminrouter.urls

router = DefaultRouter()
router.register('bizuth', views.BizuthViewset, 'bizuth')

urlpatterns = [
                  path('decharge/', views.DechargeFileUpload.as_view()),
                  path('admin/', include(admin_urls))
              ] + router.urls
