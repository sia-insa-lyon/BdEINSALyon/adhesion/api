# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, status, filters
from rest_framework.decorators import api_view, permission_classes
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from wei.models import Bizuth
from wei.serializers import PublicBizuthSerializer


class BizuthViewset(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = PublicBizuthSerializer

    # def dispatch(self, request, *args, **kwargs):
    #    if request.user.member is None:
    #        raise APIException(detail="Il faut vous inscrire à la Vie Associative avant de vous inscrire au WEI")
    #    return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return Bizuth.objects.filter(member__user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(member=self.request.user.member)

    def perform_update(self, serializer):
        serializer.save(member=self.request.user.member)

    def perform_destroy(self, instance: Bizuth):
        instance.delete()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_202_ACCEPTED, data=PublicBizuthSerializer(instance).data)


class DechargeFileUpload(APIView):
    parser_classes = [MultiPartParser]
    permission_classes = [IsAuthenticated]

    def put(self, request, format=None):
        try:
            decharge_file = request.data['file']
            request.user.member.bizuth.decharge_parentale = decharge_file
            request.user.member.bizuth.save()
            print(type(decharge_file))
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            print(type(e))
            print(e.args)
            return Response(status=status.HTTP_400_BAD_REQUEST)

