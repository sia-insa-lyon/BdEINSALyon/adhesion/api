from django.views.decorators.cache import cache_page
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.response import Response

from adhesion.api.permissions import HasRoleWei
from wei.serializers_admin import *


class AdminBizuthViewset(viewsets.ModelViewSet):
    permission_classes = [HasRoleWei]
    queryset = Bizuth.objects.all()

    filter_backends = (filters.SearchFilter, DjangoFilterBackend)

    search_fields = ("member__email", "member__first_name", "member__last_name")
    filter_fields = ('caution_valide', 'refunded', 'paiement_recu', 'participant', 'autorisation_parentale_valide')

    lookup_field = "member__id"

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return AdminBizuthReadSerializer
        elif self.action == 'create':
            return AdminBizuthCreateSerializer
        else:
            return AdminBizuthWriteSerializer

    def perform_update(self, serializer: AdminBizuthWriteSerializer):
        try:
            part: bool = serializer.validated_data['participant']
            if part:
                serializer.save()
                if Bizuth.objects.filter(participant=True).count() > settings.PLACES_WEI:
                    return serializer.save(participant=False)
                else:
                    # Le bizuth ne sera plus prioritaire s'il est de nouveau participant
                    return serializer.save(prioritaire_file=False)
        except KeyError:
            pass
        return serializer.save()

    @action(detail=False, methods=['get'], url_path='exportBizuths', url_name='export_bizuths')
    def export_bizuths(self, request):
        queryset = Bizuth.objects.all().filter(participant=True)
        return Response(ExportBizuthsSerializer(queryset, many=True).data)

    @action(detail=False, methods=['get'], url_path='exportRegimes', url_name='export_regimes')
    def export_regimes(self, request):
        queryset = Bizuth.objects.all().filter(participant=True).exclude(informations_speciales__exact='',
                                                                         regime_particulier__exact='')
        return Response(ExportRegimesSerializer(queryset, many=True).data)

    @action(detail=False, methods=['get'], url_path='tableauBizuths', url_name='tableau_bizuths')
    def tableau_bizuths(self, request):
        return Response(TableauBizuthsSerializer(Bizuth.objects.all(), many=True).data)


@cache_page(30)
@api_view()
@permission_classes([HasRoleWei])
def stats_view(request):
    places_bus = sum([b.places for b in Bus.objects.all()])
    places_bungalow = sum([b.places for b in Bungalow.objects.all()])
    remplissage_bus = 0
    remplissage_bungalow = 0
    if places_bus > 0:
        remplissage_bus = round(100 * sum([b.places_occupees for b in Bus.objects.all()]) / places_bus, 2)
    if places_bungalow > 0:
        remplissage_bungalow = round(100 * sum([b.places_occupees for b in Bungalow.objects.all()]) / places_bungalow, 2)
    return Response({
        "total_bizuths": Bizuth.objects.all().count(),
        "total_places": settings.PLACES_WEI,
        "participants": Bizuth.objects.filter(participant=True).count(),
        "remplissage_bus": remplissage_bus,
        "remplissage_bungalow": remplissage_bungalow
    })


class BusViewset(viewsets.ModelViewSet):
    permission_classes = [HasRoleWei]
    queryset = Bus.objects.all()
    serializer_class = BusSerializer

    @action(detail=True, methods=['get'])
    def bizuths(self, request, pk=None, *args, **kwargs):
        return Response(AdminBizuthSerializer(self.get_object().bizuths.filter(participant=True), many=True).data)

    @action(detail=False, methods=['get'])
    def allbizuths(self, request):
        queryset = Bizuth.objects.all().filter(participant=True, bus__isnull=False).order_by('bus')
        serializer = AdminBusBungalowList(queryset, many=True)
        return Response(serializer.data)


class BungalowViewset(viewsets.ModelViewSet):
    permission_classes = [HasRoleWei]
    queryset = Bungalow.objects.all()
    serializer_class = BungalowSerializer

    @action(detail=True, methods=['get'])
    def bizuths(self, request, pk=None, *args, **kwargs):
        return Response(AdminBizuthSerializer(self.get_object().bizuths.filter(participant=True), many=True).data)

    @action(detail=False, methods=['get'])
    def allbizuths(self, request):
        queryset = Bizuth.objects.all().filter(participant=True, bungalow__isnull=False).order_by('bungalow')
        serializer = AdminBusBungalowList(queryset, many=True)
        return Response(serializer.data)

